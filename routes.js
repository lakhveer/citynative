const bodyParser = require('body-parser');
const dotenv = require('dotenv');
dotenv.load({path: '.env'});
const express = require('express');
const app = express();
const User = require('./controllers/user');
const Lead = require('./controllers/lead');
const Agent = require('./controllers/agent');
const Referrals = require('./controllers/referrals');
const commonFunctions = require('./commonFunctions');
const adminRouting = require('./routing/admin');
const userRouting = require('./routing/user');
const leadRouting = require('./routing/lead');
const referralsRouting = require('./routing/referrals');
const agentRouting = require('./routing/agent');
var cors = require('cors');

module.exports = function(app) {
  // app.use(cors());
  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({extended:true}));
  app.use('/admin', adminRouting);
  app.use('/user', userRouting);
  app.use('/lead', leadRouting);
  app.use('/referrals', referralsRouting);
  app.use('/agent', agentRouting);
  app.get('/', function(request, response) {
      response.json('App root');
  });


  app.post('/login', function(req, res) {
     User.login(req.body).then(function(response) {
       res.status(200).send(response);
     }, function(e) {
       res.status(400).send(e);
     }).catch(function(e) {
       res.status(400).send(e);
     });
 });

  app.post('/password/reset', function(req, res){
    User.resetPassword(req.body).then(function(resetPassword){
      res.status(200).send(resetPassword)
    }, function(err){
      res.status(400).send(err);
    }).catch(function(e){
      res.status(400).send(e);
    });
  });

  app.post('/reset/change', function(req, res){
    User.resetPasswordChange(req.body).then(function(resetPassword){
      res.status(200).send(resetPassword)
    }, function(err){
      res.status(400).send(err);
    }).catch(function(e){
      res.status(400).send(e);
    });
  });

  app.post('/signup', function(req, res){
    User.signup(req.body).then(function(signup){
      res.status(200).send(signup)
    }, function(err){
      res.status(400).send(err);
    }).catch(function(e){
      res.status(400).send(e);
    });
  });

  app.post('/verifyUserEmail', function(req, res){
    User.verifyUserEmail(req.body).then(function(signup){
      res.status(200).send(signup)
    }, function(err){
      res.status(400).send(err);
    }).catch(function(e){
      res.status(400).send(e);
    });
  });

  app.post('/docusign', function(req, res){
    User.docusign(req.body).then(function(signup){
      res.status(200).send(signup)
    }, function(err){
      res.status(400).send(err);
    }).catch(function(e){
      res.status(400).send(e);
    });
  });

  app.post('/docusign/downloadDocument', function(req, res){
    User.docusigndownloadDocument(req.body).then(function(signup){
      res.status(200).send(signup)
    }, function(err){
      res.status(400).send(err);
    }).catch(function(e){
      res.status(400).send(e);
    });
  });

};
