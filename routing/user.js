const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const express = require('express');
const router = express.Router();
dotenv.load({path: '.env'});
const commonFunctions = require('./../commonFunctions');
const User = require('./../controllers/user');


router.post('/changepassword', commonFunctions.verifyAgentToken, function(req, res){
  User.changePassword(req.body).then(function(password){
    res.status(200).send(password);
  }, function(err){
    res.status(400).send(err);
  }).catch(function(e){
    res.status(400).send(e);
  });
});

module.exports = router;
