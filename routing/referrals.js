const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const express = require('express');
const router = express.Router();
dotenv.load({path: '.env'});
const commonFunctions = require('./../commonFunctions');
const Referrals = require('./../controllers/referrals');

router.post('/create', commonFunctions.verifyAgentToken, function(req, res){
  Referrals.createReferrals(req.body).then(function(lead){
    res.status(200).send(lead);
  }, function(error){
    res.status(400).send(error);
  }).catch(function(e){
    res.status(400).send(e);
  });
});

router.post('/referralBetweenAgents', commonFunctions.verifyAgentToken, function(req, res) {
  Referrals.referralAgents(req.body).then(function(response){
    res.status(200).send(response);
  },function(e){
    res.status(400).send(e);
  }).catch(function(e){
    res.status(400).send(e);
  });
});

router.post('/edit', commonFunctions.verifyAgentToken, function(req, res) {
   Referrals.editReferral(req.body).then(function(response){
     res.status(200).send(response);
   },function(e){
     res.status(400).send(e);
   }).catch(function(e){
     res.status(400).send(e);
   });
  });

router.post('/edit/status', commonFunctions.verifyAgentToken, function(req, res) {
   Referrals.editReferralStatus(req.body).then(function(response){
     res.status(200).send(response);
   },function(e){
     res.status(400).send(e);
   }).catch(function(e){
     res.status(400).send(e);
   });
  });

router.post('/showallByReferralStatus', commonFunctions.verifyAgentToken, function(req, res){
  Referrals.showallReferralStatus(req.body).then(function(lead){
    res.status(200).send(lead);
  }, function(error){
    res.status(400).send(error);
  }).catch(function(e){
    res.status(400).send(e);
  });
});

router.post('/showallBySeandingReferralAgents', commonFunctions.verifyAgentToken, function(req, res){
  Referrals.showallReferralAgent(req.body).then(function(lead){
    res.status(200).send(lead);
  }, function(error){
    res.status(400).send(error);
  }).catch(function(e){
    res.status(400).send(e);
  });
});

router.post('/showallByRecevingReferralAgents', commonFunctions.verifyAgentToken, function(req, res){
  Referrals.showallReferralRecevingAgent(req.body).then(function(lead){
    res.status(200).send(lead);
  }, function(error){
    res.status(400).send(error);
  }).catch(function(e){
    res.status(400).send(e);
  });
});

router.post('/outboundReferrals', commonFunctions.verifyAgentToken, function(req, res){
  Referrals.outboundReferrals(req.body).then(function(lead){
    res.status(200).send(lead);
  }, function(error){
    res.status(400).send(error);
  }).catch(function(e){
    res.status(400).send(e);
  });
});

router.post('/inboundReferrals', commonFunctions.verifyAgentToken, function(req, res){
  Referrals.inboundReferrals(req.body).then(function(lead){
    res.status(200).send(lead);
  }, function(error){
    res.status(400).send(error);
  }).catch(function(e){
    res.status(400).send(e);
  });
});

router.post('/action', commonFunctions.verifyAgentToken, function(req, res){
  Referrals.referralAction(req.body).then(function(lead){
    res.status(200).send(lead);
  }, function(error){
    res.status(400).send(error);
  }).catch(function(e){
    res.status(400).send(e);
  });
});

router.post('/filterByStatus', commonFunctions.verifyAgentToken, function(req, res){
  Referrals.referralFilterByStatus(req.body).then(function(lead){
    res.status(200).send(lead);
  }, function(error){
    res.status(400).send(error);
  }).catch(function(e){
    res.status(400).send(e);
  });
});

router.post('/refer/contact', commonFunctions.verifyAgentToken, function(req, res){
    Referrals.referralContactInfo(req.body).then(function(lead){
      res.status(200).send(lead);
    }, function(error){
      res.status(400).send(error);
    }).catch(function(e){
      res.status(400).send(e);
    });
  });

router.post('/refer/detail', commonFunctions.verifyAgentToken, function(req, res){
    Referrals.referralDetails(req.body).then(function(lead){
      res.status(200).send(lead);
    }, function(error){
      res.status(400).send(error);
    }).catch(function(e){
      res.status(400).send(e);
    });
  });

router.post('/export/inbound', commonFunctions.verifyAgentToken, function(req, res){
  Referrals.exportInboundDataInCSV(req.body).then(function(lead){
    res.status(200).send(lead);
  }, function(error){
    res.status(400).send(error);
  }).catch(function(e){
    res.status(400).send(e);
  });
});

router.post('/export/outbound', commonFunctions.verifyAgentToken, function(req, res){
  Referrals.exportOutboundDataInCSV(req.body).then(function(lead){
    res.status(200).send(lead);
  }, function(error){
    res.status(400).send(error);
  }).catch(function(e){
    res.status(400).send(e);
  });
});

router.post('/outbound/pendingQualification', commonFunctions.verifyAgentToken, function(req, res){
  Referrals.pendingQualification(req.body).then(function(lead){
    res.status(200).send(lead);
  }, function(error){
    res.status(400).send(error);
  }).catch(function(e){
    res.status(400).send(e);
  });
});

router.post('/outbound/activeReferrals', commonFunctions.verifyAgentToken, function(req, res){
  Referrals.activeReferrals(req.body).then(function(lead){
    res.status(200).send(lead);
  }, function(error){
    res.status(400).send(error);
  }).catch(function(e){
    res.status(400).send(e);
  });
});

router.post('/outbound/archivedReferrals', commonFunctions.verifyAgentToken, function(req, res){
  Referrals.archivedReferrals(req.body).then(function(lead){
    res.status(200).send(lead);
  }, function(error){
    res.status(400).send(error);
  }).catch(function(e){
    res.status(400).send(e);
  });
});

router.post('/remove', commonFunctions.verifyAdminToken, function(req, res){
  Referrals.removeRefferal(req.body).then(function(lead){
    res.status(200).send(lead);
  }, function(error){
    res.status(400).send(error);
  }).catch(function(e){
    res.status(400).send(e);
  });
});

router.get('/download/refferals', function(req, res){
  Referrals.downloadRefferals(req.query).then(function(lead){
     res.status(200).send(lead);
  }, function(error){
    res.status(400).send(error);
  }).catch(function(e){
    res.status(400).send(e);
  });
});

module.exports = router;
