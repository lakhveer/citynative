const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const express = require('express');
const router = express.Router();
dotenv.load({path: '.env'});
const commonFunctions = require('./../commonFunctions');
const Agent = require('./../controllers/agent');
const User = require('./../controllers/user');
const Lead = require('./../controllers/lead');
const Referrals = require('./../controllers/referrals');

router.post('/company/edit',function(req, res){
  Agent.editCompany(req).then(function(response){
    res.status(200).send(response);
  }, function(err){
    res.status(400).send(err);
  }).catch(function(e){
    res.status(400).send(e);
  });
});


router.post('/company/create', commonFunctions.verifyAgentToken, function(req, res){
  Agent.createCompany(req.body).then(function(response){
    res.status(200).send(response);
  }, function(err){
    res.status(400).send(err);
  }).catch(function(e){
    res.status(400).send(e);
  });
});

router.post('/company/delete',commonFunctions.verifyAgentToken, function(req, res){
  Agent.deleteCompany(req.body).then(function(response){
    res.status(200).send(response);
  }, function(err){
    res.status(400).send(err);
  }).catch(function(e){
    res.status(400).send(e);
  });
});

router.post('/company/detail',commonFunctions.verifyAgentToken, function(req, res){
  Agent.companyDetail(req.body).then(function(response){
    res.status(200).send(response);
  }, function(err){
    res.status(400).send(err);
  }).catch(function(e){
    res.status(400).send(e);
  });
});

router.post('/getEnvelope', commonFunctions.verifyAgentToken, function(req, res){
  Agent.getEnvelope(req.body).then(function(envelopeData){
    res.status(200).send(envelopeData);
  }, function(err){
    res.status(400).send(err);
  }).catch(function(e){
    res.status(400).send(e);
  });
});

router.post('/profile/edit',commonFunctions.verifyAgentToken, function(req, res){
  Agent.editProfile(req.body).then(function(response){
    res.status(200).send(response);
  }, function(err){
    res.status(400).send(err);
  }).catch(function(e){
    res.status(400).send(e);
  });
});

router.post('/profile/socialLinks',commonFunctions.verifyAgentToken, function(req, res){
  Agent.socialLinks(req.body).then(function(response){
    res.status(200).send(response);
  }, function(err){
    res.status(400).send(err);
  }).catch(function(e){
    res.status(400).send(e);
  });
});

router.post('/show/agent/leads', commonFunctions.verifyAgentToken, function(req, res){
  Agent.getLeadsByAgent(req.body).then(function(lead){
    res.status(200).send(lead);
  }, function(error){
    res.status(400).send(error);
  }).catch(function(e){
    res.status(400).send(e);
  });
});

router.post('/detail', commonFunctions.verifyAgentToken, function(req, res){
  User.agentDetails(req.body).then(function(agentDetails){
    res.status(200).send(agentDetails);
  }, function(error){
    res.status(400).send(error);
  }).catch(function(e){
    res.status(400).send(e);
  })
});

router.post('/showall', commonFunctions.verifyAgentToken, function(req, res) {
    User.showAllAgents(req.body).then(function(response) {
      res.status(200).send(response);
    }, function(e) {
      res.status(400).send(e);
    }).catch(function(e) {
      res.status(400).send(e);
    });
});

router.post('/lead/add', commonFunctions.verifyAgentToken, function(req, res){
    Lead.saveLead(req.body).then(function(lead){
      res.status(200).send(lead);
    }, function(error){
      res.status(400).send(error);
    }).catch(function(e){
      res.status(400).send(e);
    });
  });

  router.post('/lead/reffer', commonFunctions.verifyAgentToken, function(req, res){
      Referrals.refferLeadAgent(req.body).then(function(lead){
        res.status(200).send(lead);
      }, function(error){
        res.status(400).send(error);
      }).catch(function(e){
        res.status(400).send(e);
      });
    });

    router.post('/lead/convert', commonFunctions.verifyAgentToken, function(req, res){
        Lead.convertLeadByAgent(req.body).then(function(lead){
          res.status(200).send(lead);
        }, function(error){
          res.status(400).send(error);
        }).catch(function(e){
          res.status(400).send(e);
        });
      });
      router.post('/lead/edit', commonFunctions.verifyAgentToken, function(req, res){
          Lead.editLeadByAgent(req.body).then(function(lead){
            res.status(200).send(lead);
          }, function(error){
            res.status(400).send(error);
          }).catch(function(e){
            res.status(400).send(e);
          });
        });

module.exports = router;
