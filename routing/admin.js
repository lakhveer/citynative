  const bodyParser = require('body-parser');
  const dotenv = require('dotenv');
  const express = require('express');
  const router = express.Router();
  dotenv.load({path: '.env'});
  const commonFunctions = require('./../commonFunctions');
  const User = require('./../controllers/user');
  const Agent = require('./../controllers/agent');
  const Lead = require('./../controllers/lead');
  const Referrals = require('./../controllers/referrals');
  const Docusign = require('./../controllers/docusign');

 router.post('/agent/create', commonFunctions.verifyAdminToken, function(req, res) {
    User.createUser(req.body).then(function(response){
      res.status(200).send(response);
    },function(e){
      res.status(400).send(e);
    }).catch(function(e){
      res.status(400).send(e);
    });
  });

  router.post('/agent/showall', commonFunctions.verifyAdminToken, function(req, res) {
      User.showAllAgents(req.body).then(function(response) {
        res.status(200).send(response);
      }, function(e) {
        res.status(400).send(e);
      }).catch(function(e) {
        res.status(400).send(e);
      });
  });

  router.post('/agent/details', commonFunctions.verifyAdminToken, function(req, res){
    User.agentDetails(req.body).then(function(agentDetails){
      res.status(200).send(agentDetails);
    }, function(error){
      res.status(400).send(error);
    }).catch(function(e){
      res.status(400).send(e);
    })
  });

  router.post('/agent/edit',function(req, res){
    User.agentEditDetails(req).then(function(agentDetails){
      res.status(200).send(agentDetails);
    }, function(error){
      res.status(400).send(error);
    }).catch(function(e){
      res.status(400).send(e);
    })
  });

  router.post('/agent/remove', commonFunctions.verifyAdminToken, function(req, res){
    User.agentRemove(req.body).then(function(agentDetails){
      res.status(200).send(agentDetails);
    }, function(error){
      res.status(400).send(error);
    }).catch(function(e){
      res.status(400).send(e);
    })
  });

  router.post('/agent/update', commonFunctions.verifyAdminToken, function(req, res){
    User.updateAgent(req.body).then(function(agent){
      res.status(200).send(agent);
    }, function(error){
      res.status(400).send(error);
    }).catch(function(e){
      res.status(400).send(e);
    });
  });

  router.post('/changepassword', commonFunctions.verifyAdminToken, function(req, res){
    User.changePassword(req.body).then(function(password){
      res.status(200).send(password);
    }, function(err){
      res.status(400).send(err);
    }).catch(function(e){
      res.status(400).send(e);
    });
  });

  router.post('/agent/changepassword', commonFunctions.verifyAdminToken, function(req, res){
    User.changeAgentPassword(req.body).then(function(password){
      res.status(200).send(password);
    }, function(err){
      res.status(400).send(err);
    }).catch(function(e){
      res.status(400).send(e);
    });
  });

  router.post('/agent/leads', commonFunctions.verifyAdminToken, function(req, res){
    Agent.getAllLeads(req.body).then(function(lead){
      res.status(200).send(lead);
    }, function(error){
      res.status(400).send(error);
    }).catch(function(e){
      res.status(400).send(e);
    });
  });

  router.post('/agent/leads/status', commonFunctions.verifyAdminToken, function(req, res){
    Agent.getLeadsByAdmin(req.body).then(function(lead){
      res.status(200).send(lead);
    }, function(error){
      res.status(400).send(error);
    }).catch(function(e){
      res.status(400).send(e);
    });
  });

  router.post('/lead/assign', commonFunctions.verifyAdminToken, function(req, res){
    Lead.assignLead(req.body).then(function(lead){
      res.status(200).send(lead);
    }, function(error){
      res.status(400).send(error);
    }).catch(function(e){
      res.status(400).send(e);
    });
  });

  router.post('/lead/remove', commonFunctions.verifyAdminToken, function(req, res){
    Lead.removeLead(req.body).then(function(lead){
      res.status(200).send(lead);
    }, function(error){
      res.status(400).send(error);
    }).catch(function(e){
      res.status(400).send(e);
    });
  });

  router.post('/lead/edit', commonFunctions.verifyAdminToken, function(req, res){
    Lead.editLeadByAgent(req.body).then(function(lead){
      res.status(200).send(lead);
    }, function(error){
      res.status(400).send(error);
    }).catch(function(e){
      res.status(400).send(e);
    });
  });

  router.post('/user/changepassword', commonFunctions.verifyAgentToken, function(req, res){
    User.changePassword(req.body).then(function(password){
      res.status(200).send(password);
    }, function(err){
      res.status(400).send(err);
    }).catch(function(e){
      res.status(400).send(e);
    });
  });

  router.post('/referrals/showAll', commonFunctions.verifyAdminToken, function(req, res) {
     Referrals.showAllReferrals(req.body).then(function(response){
       res.status(200).send(response);
     },function(e){
       res.status(400).send(e);
     }).catch(function(e){
       res.status(400).send(e);
     });
   });

   router.post('/referral/edit', commonFunctions.verifyAdminToken, function(req, res) {
      Referrals.editReferral(req.body).then(function(response){
        res.status(200).send(response);
      },function(e){
        res.status(400).send(e);
      }).catch(function(e){
        res.status(400).send(e);
      });
     });

     router.post('/reffer/lead', commonFunctions.verifyAdminToken, function(req, res){
         Referrals.refferLeadAgent(req.body).then(function(lead){
           res.status(200).send(lead);
         }, function(error){
           res.status(400).send(error);
         }).catch(function(e){
           res.status(400).send(e);
         });
       });

   router.post('/docusign/integration', commonFunctions.verifyAdminToken, function(req, res) {
      Docusign.integration(req.body).then(function(response){
        res.status(200).send(response);
      },function(e){
        res.status(400).send(e);
      }).catch(function(e){
        res.status(400).send(e);
      });
    });

    router.post('/docusign/agreements/showAll', commonFunctions.verifyAdminToken, function(req, res){
      Docusign.showAllAgreements(req.body).then(function(response){
        res.status(200).send(response);
      }, function(e){
        res.status(400).send(e);
      }).catch(function(e){
        res.status(400).send(e);
      });
    });

    router.post('/manage_content/add', commonFunctions.verifyAdminToken, function(req, res){
      Docusign.manageContent(req.body).then(function(response){
        res.status(200).send(response);
      }, function(e){
        res.status(400).send(e);
      }).catch(function(e){
        res.status(400).send(e);
      });
    });

    router.post('/manage_content/edit', commonFunctions.verifyAdminToken, function(req, res){
      Docusign.editManageContent(req.body).then(function(response){
        res.status(200).send(response);
      }, function(e){
        res.status(400).send(e);
      }).catch(function(e){
        res.status(400).send(e);
      });
    });

    router.post('/manage_content/remove', commonFunctions.verifyAdminToken, function(req, res){
      Docusign.removeManageContent(req.body).then(function(response){
        res.status(200).send(response);
      }, function(e){
        res.status(400).send(e);
      }).catch(function(e){
        res.status(400).send(e);
      });
    });

    router.post('/manage_content/get', commonFunctions.verifyAdminToken, function(req, res){
      Docusign.getManageContent(req.body).then(function(response){
        res.status(200).send(response);
      }, function(e){
        res.status(400).send(e);
      }).catch(function(e){
        res.status(400).send(e);
      });
    });

    router.post('/detail', commonFunctions.verifyAdminToken, function(req, res){
      User.agentDetails(req.body).then(function(agentDetails){
        res.status(200).send(agentDetails);
      }, function(error){
        res.status(400).send(error);
      }).catch(function(e){
        res.status(400).send(e);
      })
    });

  module.exports = router;
