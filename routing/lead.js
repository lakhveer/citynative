const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const express = require('express');
const router = express.Router();
dotenv.load({path: '.env'});
const commonFunctions = require('./../commonFunctions');
const User = require('./../controllers/user');
const Lead = require('./../controllers/lead');
const Agent = require('./../controllers/agent');

router.post('/add', commonFunctions.verifyAdminToken, function(req, res){
    Lead.saveLead(req.body).then(function(lead){
      res.status(200).send(lead);
    }, function(error){
      res.status(400).send(error);
    }).catch(function(e){
      res.status(400).send(e);
    });
  });

router.post('/edit', commonFunctions.verifyAdminToken, function(req, res){
    Lead.editLead(req.body).then(function(lead){
      res.status(200).send(lead);
    }, function(error){
      res.status(400).send(error);
    }).catch(function(e){
      res.status(400).send(e);
    });
  });

router.post('/edit/status', commonFunctions.verifyAdminToken, function(req, res){
    Lead.editLeadStatus(req.body).then(function(lead){
      res.status(200).send(lead);
    }, function(error){
      res.status(400).send(error);
    }).catch(function(e){
      res.status(400).send(e);
    });
  });

  router.post('/lead/assign', commonFunctions.verifyAdminToken, function(req, res){
    Lead.assignLead(req.body).then(function(lead){
      res.status(200).send(lead);
    }, function(error){
      res.status(400).send(error);
    }).catch(function(e){
      res.status(400).send(e);
    });
  });

  router.post('/detail', commonFunctions.verifyAgentToken, function(req, res){
    Lead.leadDetail(req.body).then(function(lead){
      res.status(200).send(lead);
    }, function(error){
      res.status(400).send(error);
    }).catch(function(e){
      res.status(400).send(e);
    });
  });

  router.post('/assign', commonFunctions.verifyAgentToken, function(req, res){
    Lead.assignLead(req.body).then(function(lead){
      res.status(200).send(lead);
    }, function(error){
      res.status(400).send(error);
    }).catch(function(e){
      res.status(400).send(e);
    });
  });

  router.post('/status', commonFunctions.verifyAgentToken, function(req, res){
    Agent.getLeadsByAgentStatus(req.body).then(function(lead){
      res.status(200).send(lead);
    }, function(error){
      res.status(400).send(error);
    }).catch(function(e){
      res.status(400).send(e);
    });
  });

module.exports = router;
