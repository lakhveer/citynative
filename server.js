const express = require('express');
const path = require('path');
const app = express();
require('dotenv').config();
const port = process.env.PORT;
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

require('./routes.js')(app);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/public/tmp', express.static(path.join(__dirname, './public/tmp')));

var uri = process.env.DB_URL + process.env.DB_NAME;
mongoose.connection.openUri(uri, function (err, res) {
  if (err) {
      console.log ('Error connecting to: ' + uri + '. ' + err);
  } else {
      console.log ('Connected to: ' + uri);
  }
});

app.listen(port, function() {
    console.log('Node app is runnning on PORT: ' + port);
});
