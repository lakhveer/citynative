const mongoose = require('mongoose');

const propertySchema = new mongoose.Schema({
  mls: { type: String },
  address: { type: String },
  city: { type: String },
  state: { type: String },
  zip: { type: String },
  ownerId: { type: String, ref: 'User' },
  receivingAgentId: { type: Array, ref: 'User' },
  status: { type: String, enum: ['Qualified', 'Unqualified','Pending Qualification'] }
});

const referralsSchema = new mongoose.Schema({
  firstName: { type: String, required: true },
  lastName: { type: String },
  email: { type: String, required: true, index: { unique: true } },
  company: { type: String },
  phone: { type: String },
  date: { type: Date, default: Date.now },
  location: { type: String },
  opportunity: { type: String },
  properties: [propertySchema],
  modifyDate: { type: Date, default: Date.now },
  source: { type: String },
  message: { type: String },
  refAddress: { type: String },
  referralStatus: { type: String, enum: ['Pending','Prospects','Clients','Under Contract','Closed','Nurture','Cancelled'] }
});
//mongoose.set('debug', true);
module.exports = mongoose.model('Referrals', referralsSchema);
