const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;

const socialSchema = new mongoose.Schema({
  facebook: { type: String },
  twitter: { type: String },
  linkedin: { type: String },
  instagram: { type: String }
});

const propertyType = new mongoose.Schema({
  for: { type: String },
  property: { type: String, enum: ['Single Family','Condo','Residential','Commercial'] }
});

const userSchema = new mongoose.Schema({
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  email: { type: String, required: true, index: { unique: true } },
  password: { type: String, required: true },
  role: { type: String, required: true },
  company: { type: String },
  passwordToken: { type: String },
  streetNumber: { type: String },
  streetName: { type: String },
  city: { type: String },
  state: { type: String },
  zip: { type: String },
  phone: { type: String },
  profileImage: { type: String },
  title: { type: String },
  licenseType: { type: String },
  mobile: { type: String },
  yearsInRealEstate: { type: String, enum: ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22'
  ,'23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47'
  ,'48','49','50+'] },
  transactionsCompleted: { type: String, enum: ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22'
  ,'23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47'
  ,'48','49','50+'] },
  expertise: [propertyType],
  professionalAssociations: { type: String, enum: ['A','B','C','D'] },
  trainingCertifications: { type: String, enum: ['A','B','C','D'] },
  languages: { type: String, enum: ['English','French','German'] },
  areasOfService: { type: String },
  about: { type: String },
  shortSaleExperience: { type: String },
  preferences: { type: String, enum: ['Buyers','Investors','Sellers','Short Sales'] },
  socialLinks: socialSchema,
  envelopeId : { type: String },
  verify : { type: String },
  createAccount : { type: String },
/*  signDocumentUrl : { type: String },
    location : {
    type: {type:String}, coordinates: [Number]
  }, */
  /*leads: [ { type: Array, ref: 'Lead' } ]*/
});

userSchema.pre('save', function(next){
    var user = this;
    if (!user.isModified('password')) return next();
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt){
        if(err) return next(err);
        bcrypt.hash(user.password, salt, function(err, hash){
            if(err) return next(err);
            user.password = hash;
            next();
        });
    });
});
// mongoose.set('debug', true);
//userSchema.index( { location : "2dsphere" } );

module.exports = mongoose.model('User', userSchema);
