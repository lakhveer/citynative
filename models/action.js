const mongoose = require('mongoose');

const subactionSchema = new mongoose.Schema({
  name: { type: String },
  detail: { type: String },
  date: { type: Date, default: Date.now }
});

const actionSchema = new mongoose.Schema({
  referralId: { type: String },
  mlsNo: { type: String },
  agentId: { type: String },
  actions: [subactionSchema]
});

module.exports = mongoose.model('Action', actionSchema);
