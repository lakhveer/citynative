const mongoose = require('mongoose');

const companySchema = new mongoose.Schema({
  companyName: { type: String, required: true },
  brokerageName: { type: String },
  licenseExpireDate: { type: String },
  phoneNumber: { type: String },
  fax: { type: String },
  address: { type: String },
  city: { type: String },
  state: { type: String },
  zip: { type: String },
  companyLogo: { type: String },
  agentId: { type: String, required: true },
});

module.exports = mongoose.model('Company', companySchema);
