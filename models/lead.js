const mongoose = require('mongoose');

const propertySchema = new mongoose.Schema({
  mls: { type: String },
  address: { type: String },
  city: { type: String },
  state: { type: String },
  zip: { type: String },
  reason : { type: String },
  agentId: { type: String, ref: 'User'},
  status: { type: String, enum: ['Qualified','Unqualified','Pending Qualification'] },
  // location : { type: String, coordinates: {type:Array}, default:[0,0] },
});

const leadSchema = new mongoose.Schema({
  firstName: { type: String, required: true },
  lastName: { type: String },
  email: { type: String, required: true, index: { unique: true } },
  company: { type: String },
  phone: { type: String },
  date: { type: Date, default: Date.now },
  modifyDate: { type: Date, default: Date.now },
  source: { type: String },
  message: { type: String },
  leadAddress: { type: String },
  leadStatus: { type: String, enum: ['Qualified','Unqualified','Pending'] },
  properties: [propertySchema],
  status: { type: String } 
});

propertySchema.index( { location : "2dsphere" } );
// mongoose.set('debug', true);
module.exports = mongoose.model('Lead', leadSchema);
