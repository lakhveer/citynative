const mongoose = require('mongoose');

const docusignSchema = new mongoose.Schema({
  ownerId: { type: String, ref: 'User' },
  email: { type: String, required: true },
  password: { type: String, required: true },
  apiKey: { type: String, required: true },
  templateId: { type: String },
  templateRoleName: { type: String }
});

module.exports = mongoose.model('Docusign', docusignSchema);
