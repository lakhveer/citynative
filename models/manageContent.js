const mongoose = require('mongoose');

const manageContent = new mongoose.Schema({
  name: { type: String },
  description: { type: String },
});

module.exports = mongoose.model('manageContent', manageContent);
