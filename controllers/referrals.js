  'use strict';

  const _ = require('underscore');
  const Referrals = require('./../models/referrals');
  const Action = require('./../models/action');
  const User = require('./../models/user');
  const bcrypt = require('bcrypt');
  var randtoken = require('random-token');
  const SALT_WORK_FACTOR = 10;
  const commonFunctions = require('./../commonFunctions.js');
  require('dotenv').config();
// const URL = "https://api.citynative.com/";
const URL = "http://localhost:3000/";
  var express = require('express');
  const path = require('path');

  var json2csv = require('json2csv');
  var fs = require('fs');

  var fields = ['_id', 'company', 'email', 'firstName', 'lastName', 'phone', 'referralStatus', 'date', 'location', 'properties[0].address', 'properties[0].city', 'properties[0].mls', 'properties[0].state', 'properties[0].status', 'properties[0].zip', 'properties[0].ownerId', 'properties[0].receivingAgentId'];

  module.exports = {
    createReferrals,
    showallReferralStatus,
    showallReferralAgent,
    showallReferralRecevingAgent,
    showAllReferrals,
    referralAgents,
    editReferral,
    editReferralStatus,
    outboundReferrals,
    inboundReferrals,
    referralAction,
    referralFilterByStatus,
    referralContactInfo,
    referralDetails,
    exportInboundDataInCSV,
    exportOutboundDataInCSV,
    pendingQualification,
    activeReferrals,
    archivedReferrals,
    removeRefferal,
    refferLeadAgent,
    downloadRefferals
  };

  function createReferrals(body) {
    return new Promise(function(resolve, reject) {
      var newBody = _.pick(body, 'firstName', 'email', 'properties');
      if (!newBody.hasOwnProperty('firstName') || !newBody.hasOwnProperty('email') || newBody.email === '' || newBody.firstName === '' || newBody.email.trim().length <= 0 || newBody.firstName.trim().length <= 0 || newBody.properties.length !== undefined) {
        return reject(commonFunctions.sendErrorResponse('Invalid or missing parameters'));
      } else {
        var query = {
          email: newBody.email
        };
        Referrals.findOne(query, (err, refer) => {
          if (err) {
            return reject(commonFunctions.sendErrorResponse(err));
          } else {
            if (refer) {
              Referrals.update({
                'email': newBody.email,
                'properties.mls': {
                  $ne: newBody.properties.mls
                }
              }, {
                $push: {
                  properties: newBody.properties
                }
              }, (err, updatedRefer) => {
                if (err) {
                  return reject(commonFunctions.sendErrorResponse(err));
                } else {
                  return resolve(commonFunctions.sendSuccessResponse(updatedRefer));
                }
              });
            } else {
              var referrals = new Referrals(body);
              referrals.save((err, newRefrer) => {
                if (err) {
                  return reject(commonFunctions.sendErrorResponse(err));
                } else {
                  return resolve(commonFunctions.sendSuccessResponse("New Refferal create successfully"));
                }
              });
            }
          }
        });
      }
    });
  }

  function showallReferralStatus(body) {
    return new Promise(function(resolve, reject) {
      var newBody = _.pick(body, 'status');
      if (!newBody.hasOwnProperty('status') || newBody.status === '' || newBody.status.trim().length <= 0) {
        return reject(commonFunctions.sendErrorResponse('Invalid or missing parameters'));
      } else {
        Referrals.find({
          'referralStatus': newBody.status
        }, function(err, res) {
          if (err) {
            return reject(commonFunctions.sendErrorResponse(err));
          } else {
            if (res.length <= 0) {
              return reject(commonFunctions.sendErrorResponse('No referrals found'));
            } else {
              return resolve(commonFunctions.sendSuccessResponse(res));
            }
          }
        });
      }
    });
  }

  function showallReferralAgent(body) {
    return new Promise(function(resolve, reject) {
      var newBody = _.pick(body, 'agentId');
      if (!newBody.hasOwnProperty('agentId') || newBody.agentId === '' || newBody.agentId.trim().length <= 0) {
        return reject(commonFunctions.sendErrorResponse('Invalid or missing parameters'));
      } else {
        Referrals.find({
          'properties.ownerId': newBody.agentId
        }, function(err, res) {
          if (err) {
            return reject(commonFunctions.sendErrorResponse(err));
          } else {
            if (res.length <= 0) {
              return reject(commonFunctions.sendErrorResponse('No referrals found'));
            } else {
              var recAgentId = newBody.agentId;
              for (var z = 0; z < res.length; z++) {
                var finalArray = [];
                for (var i = 0; i < res[z].properties.length; i++) {
                  var props = res[z].properties[i].ownerId;
                  if (props.indexOf(recAgentId) > -1) {
                    finalArray.push(res[z].properties[i]);
                  }
                }
                res[z].properties = finalArray;
              }
              return resolve(commonFunctions.sendSuccessResponse(res));
            }
          }
        });
      }
    });
  }

  function showallReferralRecevingAgent(body) {
    return new Promise(function(resolve, reject) {
      var newBody = _.pick(body, 'agentId');
      if (!newBody.hasOwnProperty('agentId') || newBody.agentId === '' || newBody.agentId.trim().length <= 0) {
        return reject(commonFunctions.sendErrorResponse('Invalid or missing parameters'));
      } else {
        Referrals.find({
          'properties.receivingAgentId': {
            $in: [newBody.agentId]
          }
        }, function(err, res) {
          if (err) {
            return reject(commonFunctions.sendErrorResponse(err));
          } else {
            if (res.length <= 0) {
              return reject(commonFunctions.sendErrorResponse('No referrals found'));
            } else {
              var recAgentId = newBody.agentId;
              for (var z = 0; z < res.length; z++) {
                var finalArray = [];
                for (var i = 0; i < res[z].properties.length; i++) {
                  var props = res[z].properties[i].receivingAgentId;
                  if (props.indexOf(recAgentId) > -1) {
                    finalArray.push(res[z].properties[i]);
                  }
                }
                res[z].properties = finalArray;
              }
              return resolve(commonFunctions.sendSuccessResponse(res));
            }
          }
        });
      }
    });
  }

  function showAllReferrals(body) {
    return new Promise(function(resolve, reject) {
      Referrals.find(function(err, res) {
        if (err) {
          return reject(commonFunctions.sendErrorResponse(err));
        } else {
          if (res.length <= 0) {
            return reject(commonFunctions.sendErrorResponse('No referrals found'));
          } else {
            return resolve(commonFunctions.sendSuccessResponse(res));
          }
        }
      });
    });
  }

  function referralAgents(body) {
    return new Promise(function(resolve, reject) {
      var newBody = _.pick(body, 'ownerId', 'receveAgentId');
      if (!newBody.hasOwnProperty('ownerId') || newBody.ownerId === '' || newBody.ownerId.trim().length <= 0 ||
        !newBody.hasOwnProperty('receveAgentId') || newBody.receveAgentId === '' || newBody.receveAgentId.trim().length <= 0) {
        return reject(commonFunctions.sendErrorResponse('Invalid or missing parameters'));
      } else {
        Referrals.find({
          'properties.ownerId': newBody.ownerId
        }, function(err, res) {
          if (err) {
            return reject(commonFunctions.sendErrorResponse(err));
          } else {
            if (res.length <= 0) {
              return reject(commonFunctions.sendErrorResponse('ownerId not found'));
            } else {
              Referrals.find({
                'properties.receivingAgentId': newBody.receveAgentId
              }, function(err, res) {
                if (res.length <= 0) {
                  return reject(commonFunctions.sendErrorResponse('Recever agent id not found'));
                } else {
                  Referrals.find({
                    'properties.ownerId': newBody.ownerId
                  }, function(err, res) {
                    if (err) {
                      return reject(commonFunctions.sendErrorResponse(err));
                    } else {
                      var recAgentId = newBody.receveAgentId;
                      for (var z = 0; z < res.length; z++) {
                        var finalArray = [];
                        for (var i = 0; i < res[z].properties.length; i++) {
                          var props = res[z].properties[i].receivingAgentId;
                          if (props.indexOf(recAgentId) > -1) {
                            finalArray.push(res[z].properties[i]);
                          }
                        }
                        res[z].properties = finalArray;
                      }
                      return resolve(commonFunctions.sendSuccessResponse(res));
                    }
                  });
                }
              }).select();
            }
          }
        });
      }
    });
  }

  function editReferral(body) {
    return new Promise(function(resolve, reject) {
      var newBody = _.pick(body, 'email', 'firstName');
      if (!newBody.hasOwnProperty('email') || newBody.email === '' || newBody.email.trim().length <= 0 ||
        !newBody.hasOwnProperty('firstName') || newBody.firstName === '' || newBody.firstName.trim().length <= 0) {
        return reject(commonFunctions.sendErrorResponse('Invalid or missing parameters'));
      } else {
        var query = {
          email: newBody.email
        };
        Referrals.findOneAndUpdate(query, {
          $set: body
        }, {
          new: true
        }, function(err, refer) {
          if (err) {
            return reject(commonFunctions.sendErrorResponse(err));
          } else {
            return resolve(commonFunctions.sendSuccessResponse("Refferal successfully updated"));
          }
        });
      }
    });
  }

  function editReferralStatus(body) {
    return new Promise(function(resolve, reject) {
      var newBody = _.pick(body, 'referId', 'referralStatus');
      if (!newBody.hasOwnProperty('referId') || newBody.referId === '' || newBody.referId.trim().length <= 0 ||
        !newBody.hasOwnProperty('referralStatus') || newBody.referralStatus === '' || newBody.referralStatus.trim().length <= 0) {
        return reject(commonFunctions.sendErrorResponse('Invalid or missing parameters'));
      } else {
        Referrals.find({
          '_id': newBody.referId
        }, function(err, res) {
          if (err) {
            return reject(commonFunctions.sendErrorResponse('refer id not found'));
          } else {
            if (res.length <= 0) {
              return reject(commonFunctions.sendErrorResponse('refer id not found'));
            } else {
              var query = {
                _id: newBody.referId
              };
              Referrals.findOneAndUpdate(query, {
                $set: {
                  'referralStatus': newBody.referralStatus
                }
              }, function(err, refer) {
                if (res.length <= 0) {
                  return reject(commonFunctions.sendErrorResponse('refer id not found'));
                } else {
                  User.find({
                    'role': 'admin'
                  }, function(err, res) {
                    if (err) {
                      return reject(commonFunctions.sendErrorResponse(err));
                    } else {
                      var adminEmail = res[0].email;
                      var message = {
                        to: [refer.email, adminEmail],
                        from: process.env.SENDGRID_API_EMAIL,
                        subject: 'Update Referral Status',
                        text: 'test',
                        html: "<p>Dear " + refer.firstName + ",</p><p>Referral status change <b>" + newBody.referralStatus + "</b>.</p>",
                      };
                      commonFunctions.sendGridEmail(message).then(function(emailSent) {
                        return resolve(commonFunctions.sendSuccessResponse(emailSent));
                      }, function(error) {
                        return reject(commonFunctions.sendErrorResponse(error));
                      }).catch(function(e) {
                        return reject(commonFunctions.sendErrorResponse(e));
                      });
                      return resolve(commonFunctions.sendSuccessResponse('referral status update'));
                    }
                  });
                }
              });
            }
          }
        });
      }
    });
  }

  function outboundReferrals(body) {
    return new Promise(function(resolve, reject) {
      var newBody = _.pick(body, 'agentId', 'referralStatus');
      if (!newBody.hasOwnProperty('agentId') || newBody.agentId === '' || newBody.agentId.trim().length <= 0 ||
        !newBody.hasOwnProperty('referralStatus') || newBody.referralStatus === '' || newBody.referralStatus.trim().length <= 0) {
        return reject(commonFunctions.sendErrorResponse('Invalid or missing parameters'));
      } else {
        Referrals.find({$and :[
          {'properties.ownerId': newBody.agentId},
          {'referralStatus': newBody.referralStatus}
        ]}, function(err, res) {
          if (err) {
            return reject(commonFunctions.sendErrorResponse(err));
          } else {
            if (res.length <= 0) {
              return resolve(commonFunctions.sendErrorResponse('No record found'));
            } else {
            /*  var recAgentId = newBody.agentId;
              for (var z = 0; z < res.length; z++) {
                var finalArray = [];
                for (var i = 0; i < res[z].properties.length; i++) {
                  var props = res[z].properties[i].ownerId;
                  if (props.indexOf(recAgentId) > -1) {
                    finalArray.push(res[z].properties[i]);
                  }
                }
                res[z].properties = finalArray;
              } */
              return resolve(commonFunctions.sendSuccessResponse(res));
            }
          }
        });
      }
    });
  }

  function inboundReferrals(body) {
    return new Promise(function(resolve, reject) {
      var newBody = _.pick(body, 'agentId', 'referralStatus');
      if (!newBody.hasOwnProperty('agentId') || newBody.agentId === '' || newBody.agentId.trim().length <= 0 ||
        !newBody.hasOwnProperty('referralStatus') || newBody.referralStatus === '' || newBody.referralStatus.trim().length <= 0) {
        return reject(commonFunctions.sendErrorResponse('Invalid or missing parameters'));
      } else {
        var cer={
          referralStatus: newBody.referralStatus,
          properties: {
            '$elemMatch': {
              receivingAgentId:newBody.agentId
            }
          }
        }
        Referrals.find(cer, function(err, res) {
          if (res.length <= 0) {
            return resolve(commonFunctions.sendSuccessResponse('No record found'));
          } else {
            var recAgentId = newBody.agentId;
            for (var z = 0; z < res.length; z++) {
              var finalArray = [];
              for (var i = 0; i < res[z].properties.length; i++) {
                var props = res[z].properties[i].receivingAgentId;
                if (props.indexOf(recAgentId) > -1) {
                  finalArray.push(res[z].properties[i]);
                }
              }
              res[z].properties = finalArray;
            }
            return resolve(commonFunctions.sendSuccessResponse(res));
          }
        });
      }
    });
  }

  function referralAction(body) {
    return new Promise(function(resolve, reject) {
      var newBody = _.pick(body, 'referralId', 'agentId', 'actions');
      if (!newBody.hasOwnProperty('referralId') || newBody.referralId === '' || newBody.referralId.trim().length <= 0 ||
        !newBody.hasOwnProperty('agentId') || newBody.agentId === '' || newBody.agentId.trim().length <= 0 ||
        !newBody.hasOwnProperty('actions') || newBody.actions === '' || newBody.actions.length != undefined) {
        return reject(commonFunctions.sendErrorResponse('Invalid or missing parameters'));
      } else {
        Referrals.find({
          '_id': newBody.referralId
        }, function(err, res) {
          if (err) {
            return reject(commonFunctions.sendErrorResponse('referralId not found'));
          } else {
            if (res.length <= 0) {
              return reject(commonFunctions.sendErrorResponse('referralId not found'));
            } else {
                var action = new Action(newBody);
                action.save((err, newAction) => {
                  if (err) {
                    return reject(commonFunctions.sendErrorResponse(err));
                  } else {
                    return resolve(commonFunctions.sendSuccessResponse(newAction));
                  }
                });
            }
          }
        });
      }
    });
  }

  function referralFilterByStatus(body) {
    return new Promise(function(resolve, reject) {
      var newBody = _.pick(body, 'agentId', 'referralStatus');
      if (!newBody.hasOwnProperty('agentId') || newBody.agentId === '' || newBody.agentId.trim().length <= 0 || !newBody.hasOwnProperty('referralStatus')) {
        return reject(commonFunctions.sendErrorResponse('Invalid or missing parameters'));
      } else {
        Referrals.find({
          'properties.ownerId': newBody.agentId
        }, function(err, res) {
          if (err) {
            return reject(commonFunctions.sendErrorResponse(err));
          } else {
            if (res.length <= 0) {
              return reject(commonFunctions.sendErrorResponse('agent id not found'));
            } else {
              if (newBody.referralStatus == "") {
                Referrals.find({
                  'properties.ownerId': newBody.agentId
                }, function(err, res) {
                  if (err) {
                    return reject(commonFunctions.sendErrorResponse(err));
                  } else {
                    var ownerId = newBody.agentId;
                    for (var z = 0; z < res.length; z++) {
                      var finalArray = [];
                      for (var i = 0; i < res[z].properties.length; i++) {
                        var props = res[z].properties[i].ownerId;
                        if (props.indexOf(ownerId) > -1) {
                          finalArray.push(res[z].properties[i]);
                        }
                      }
                      res[z].properties = finalArray;
                    }
                    return resolve(commonFunctions.sendSuccessResponse(res));
                  }
                });
              } else {
                Referrals.find({
                  $and: [{
                    'referralStatus': newBody.referralStatus,
                    'properties.ownerId': newBody.agentId
                  }]
                }, function(err, res) {
                  if (err) {
                    return reject(commonFunctions.sendErrorResponse(err));
                  } else {
                    if (res.length <= 0) {
                      return resolve(commonFunctions.sendSuccessResponse('no result found'));
                    } else {
                      var ownerId = newBody.agentId;
                      for (var z = 0; z < res.length; z++) {
                        var finalArray = [];
                        for (var i = 0; i < res[z].properties.length; i++) {
                          var props = res[z].properties[i].ownerId;
                          if (props.indexOf(ownerId) > -1) {
                            finalArray.push(res[z].properties[i]);
                          }
                        }
                        res[z].properties = finalArray;
                      }
                      return resolve(commonFunctions.sendSuccessResponse(res));
                    }
                  }
                });
              }
            }
          }
        });
      }
    });
  }

  function referralContactInfo(body) {
    return new Promise(function(resolve, reject) {
      var newBody = _.pick(body, 'referAgentId');
      if (!newBody.hasOwnProperty('referAgentId') || newBody.referAgentId === '' || newBody.referAgentId.trim().length <= 0) {
        return reject(commonFunctions.sendErrorResponse('Invalid or missing parameters'));
      } else {
        Referrals.find({
          'properties.receivingAgentId': {
            $in: [newBody.referAgentId]
          }
        }, function(err, res) {
          if (err) {
            return reject(commonFunctions.sendErrorResponse(err));
          } else {
            if (res.length <= 0) {
              return resolve(commonFunctions.sendSuccessResponse('referAgentId not found'));
            } else {
              Referrals.find({
                'properties.receivingAgentId': {
                  $in: [newBody.referAgentId]
                }
              }, function(err, res) {
                if (err) {
                  return reject(commonFunctions.sendErrorResponse(err));
                } else {
                  return resolve(commonFunctions.sendSuccessResponse(res));
                }
              }).select('email firstName phone');
            }
          }
        });
      }
    });
  }

  function referralDetails(body) {
    return new Promise(function(resolve, reject) {
      var newBody = _.pick(body, 'referId');
      if (!newBody.hasOwnProperty('referId') || newBody.referId === '' || newBody.referId.trim().length <= 0) {
        return reject(commonFunctions.sendErrorResponse('Invalid or missing parameters'));
      } else {
        Referrals.find({
          '_id': newBody.referId
        }, function(err, res) {
          if (err) {
            return reject(commonFunctions.sendErrorResponse(err));
          } else {
            if (res.length <= 0) {
              return resolve(commonFunctions.sendSuccessResponse('Refer id not found'));
            } else {
              return resolve(commonFunctions.sendSuccessResponse(res));
            }
          }
        });
      }
    });
  }

  function exportInboundDataInCSV(body) {
    return new Promise(function(resolve, reject) {
      var newBody = _.pick(body, 'agentId');
      if (!newBody.hasOwnProperty('agentId') || newBody.agentId === '' || newBody.agentId.trim().length <= 0) {
        return reject(commonFunctions.sendErrorResponse('Invalid or missing parameters'));
      } else {
        Referrals.find({
          'properties.receivingAgentId': {
            $in: [newBody.agentId]
          }
        }, function(err, res) {
          var recAgentId = newBody.agentId;
          for (var z = 0; z < res.length; z++) {
            var finalArray = [];
            for (var i = 0; i < res[z].properties.length; i++) {
              var props = res[z].properties[i].receivingAgentId;
              if (props.indexOf(recAgentId) > -1) {
                finalArray.push(res[z].properties[i]);
              }
            }
            res[z].properties = finalArray;
          }
          var date = new Date();
          var randName = randtoken(8);
          var d = randName + '_' + date.getFullYear() + "_" + (date.getMonth() + 1) + "_" + date.getDate();
          var csv = json2csv({
            data: res,
            fields: fields,
            unwindPath: ['properties', 'properties.receivingAgentId']
          });
          fs.writeFile('public/tmp/inbound_referrals_' + d + '.csv', csv, function(err) {
            if (err) {
              return reject(commonFunctions.sendErrorResponse(err));
            } else {
              var file = path.join(__dirname, '..', './public/tmp/inbound_referrals_' + d + '.csv')
              return resolve(commonFunctions.sendSuccessResponse(URL+'public/tmp/inbound_referrals_' + d + '.csv'));
            }
          });
        });
      }
    });
  }

  function exportOutboundDataInCSV(body) {
    return new Promise(function(resolve, reject) {
      var newBody = _.pick(body, 'agentId');
      if (!newBody.hasOwnProperty('agentId') || newBody.agentId === '' || newBody.agentId.trim().length <= 0) {
        return reject(commonFunctions.sendErrorResponse('Invalid or missing parameters'));
      } else {
        Referrals.find({
          'properties.ownerId': newBody.agentId
        }, function(err, res) {
          if (err) {
            return reject(commonFunctions.sendErrorResponse(err));
          } else {
            if (res.length <= 0) {
              return reject(commonFunctions.sendErrorResponse('agentId not found'));
            } else {
              var recAgentId = newBody.agentId;
              for (var z = 0; z < res.length; z++) {
                var finalArray = [];
                for (var i = 0; i < res[z].properties.length; i++) {
                  var props = res[z].properties[i].ownerId;
                  if (props.indexOf(recAgentId) > -1) {
                    finalArray.push(res[z].properties[i]);
                  }
                }
                res[z].properties = finalArray;
              }
              var date = new Date();
              var d = date.getFullYear() + "_" + (date.getMonth() + 1) + "_" + date.getDate();
              var csv = json2csv({
                data: res,
                fields: fields,
                unwindPath: ['properties', 'properties.receivingAgentId']
              });
              fs.writeFile('public/tmp/outbound_referrals_' + d + '.csv', csv, function(err) {
                if (err) {
                  return reject(commonFunctions.sendErrorResponse(err));
                }else{
                  var file = path.join(__dirname, '..', './public/tmp/outbound_referrals_' + d + '.csv')
                  return resolve(commonFunctions.sendSuccessResponse(URL+'public/tmp/outbound_referrals_' + d + '.csv'));
                }
              });
            }
          }
        });
      }
    });
  }

  function pendingQualification(body) {
    return new Promise(function(resolve, reject) {
      var newBody = _.pick(body, 'agentId');
      if (!newBody.hasOwnProperty('agentId') || newBody.agentId === '' || newBody.agentId.trim().length <= 0) {
        return reject(commonFunctions.sendErrorResponse('Invalid or missing parameters'));
      } else {
        Referrals.find({
          'properties.receivingAgentId': {
            $in: [newBody.agentId]
          }
        }, function(err, res) {
          if (err) {
            return reject(commonFunctions.sendErrorResponse(err));
          } else {
            if (res.length <= 0) {
              return reject(commonFunctions.sendErrorResponse('agentId not found'));
            } else {
              Referrals.find({
                $and: [{
                  'properties.receivingAgentId': {
                    $in: [newBody.agentId]
                  },
                  'referralStatus': 'Pending'
                }]
              }, function(err, res) {
                if (res.length <= 0) {
                  return reject(commonFunctions.sendErrorResponse('no data found'));
                } else {
                  return resolve(commonFunctions.sendSuccessResponse(res));
                }
              });
            }
          }
        });
      }
    });
  }

  function activeReferrals(body) {
    return new Promise(function(resolve, reject) {
      var newBody = _.pick(body, 'agentId', 'referralStatus');
      if (!newBody.hasOwnProperty('agentId') || newBody.agentId === '' || newBody.agentId.trim().length <= 0 ||
        !newBody.hasOwnProperty('referralStatus') || newBody.referralStatus === '' || newBody.referralStatus.trim().length <= 0) {
        return reject(commonFunctions.sendErrorResponse('Invalid or missing parameters'));
      } else {
        Referrals.find({
          'properties.receivingAgentId': {
            $in: [newBody.agentId]
          }
        }, function(err, res) {
          if (err) {
            return reject(commonFunctions.sendErrorResponse(err));
          } else {
            if (res.length <= 0) {
              return resolve(commonFunctions.sendSuccessResponse('agent id not found'));
            } else {
              Referrals.find({
                $and: [{
                  'properties.receivingAgentId': {
                    $in: [newBody.agentId]
                  },
                  'referralStatus': newBody.referralStatus
                }]
              }, function(err, res) {
                if (res.length <= 0) {
                  return resolve(commonFunctions.sendSuccessResponse('no data found'));
                } else {
                  var recAgentId = newBody.agentId;
                  for (var z = 0; z < res.length; z++) {
                    var finalArray = [];
                    for (var i = 0; i < res[z].properties.length; i++) {
                      var props = res[z].properties[i].receivingAgentId;
                      if (props.indexOf(recAgentId) > -1) {
                        finalArray.push(res[z].properties[i]);
                      }
                    }
                    res[z].properties = finalArray;
                  }
                  return resolve(commonFunctions.sendSuccessResponse(res));
                }
              });
            }
          }
        });
      }
    });
  }

  function archivedReferrals(body) {
    return new Promise(function(resolve, reject) {
      var newBody = _.pick(body, 'agentId', 'referralStatus');
      if (!newBody.hasOwnProperty('agentId') || newBody.agentId === '' || newBody.agentId.trim().length <= 0 ||
        !newBody.hasOwnProperty('referralStatus') || newBody.referralStatus === '' || newBody.referralStatus.trim().length <= 0) {
        return reject(commonFunctions.sendErrorResponse('Invalid or missing parameters'));
      } else {
        Referrals.find({
          'properties.receivingAgentId': {
            $in: [newBody.agentId]
          }
        }, function(err, res) {
          if (err) {
            return reject(commonFunctions.sendErrorResponse(err));
          } else {
            if (res.length <= 0) {
              return resolve(commonFunctions.sendSuccessResponse('agent id not found'));
            } else {
              Referrals.find({
                $and: [{
                  'properties.receivingAgentId': {
                    $in: [newBody.agentId]
                  },
                  'referralStatus': newBody.referralStatus
                }]
              }, function(err, res) {
                if (res.length <= 0) {
                  return resolve(commonFunctions.sendSuccessResponse('no data found'));
                } else {
                  var recAgentId = newBody.agentId;
                  for (var z = 0; z < res.length; z++) {
                    var finalArray = [];
                    for (var i = 0; i < res[z].properties.length; i++) {
                      var props = res[z].properties[i].receivingAgentId;
                      if (props.indexOf(recAgentId) > -1) {
                        finalArray.push(res[z].properties[i]);
                      }
                    }
                    res[z].properties = finalArray;
                  }
                  return resolve(commonFunctions.sendSuccessResponse(res));
                }
              });
            }
          }
        });
      }
    });
  }

  function removeRefferal(body) {
    return new Promise(function(resolve, reject) {
      var newBody = _.pick(body, 'refferId');
      if (!newBody.hasOwnProperty('refferId') || newBody.refferId === '' || newBody.refferId.trim().length <= 0) {
        return reject(commonFunctions.sendErrorResponse('Invalid or missing parameters'));
      } else {
        Referrals.remove({
          _id: newBody.refferId
        }, (err, res) => {
          if (err) {
            return reject(commonFunctions.sendErrorResponse(err));
          } else {
            Referrals.find({}, (err, res) => {
              if (err) {
                return reject(commonFunctions.sendErrorResponse(err));
              } else {
                return resolve(commonFunctions.sendSuccessResponse(res));
              }
            });
          }
        });
      }
    });
  }

  function refferLeadAgent(body) {
    return new Promise(function(resolve, reject) {
      var newBody = _.pick(body, 'refferId', 'agentId', 'mlsNo');
      if (!newBody.hasOwnProperty('refferId') || newBody.refferId === '' || newBody.refferId.trim().length <= 0 ||
        !newBody.hasOwnProperty('agentId') || newBody.agentId === '' || newBody.agentId.trim().length <= 0 ||
        !newBody.hasOwnProperty('mlsNo') || newBody.mlsNo === '' || newBody.mlsNo.trim().length <= 0) {
        return reject(commonFunctions.sendErrorResponse('Invalid or missing parameters'));
      } else {
        var query = {
          '_id': newBody.refferId,
          'properties.mls': newBody.mlsNo
        };
        Referrals.findOneAndUpdate(query, {
          $set: {
            'properties.$.receivingAgentId': [body.refferAgentId],
            'properties.$.ownerId': '',
            'referralStatus': body.referralStatus
          }
        }, {
          new: true
        }, (err, res) => {
          if (err) {
            return reject(commonFunctions.sendErrorResponse(err));
          } else {
            return resolve(commonFunctions.sendSuccessResponse(res));
          }
        });
      }
    });
  }

  function downloadRefferals(body) {
    return new Promise(function(resolve, reject) {
      var file = path.join(__dirname, '..', './public/tmp/' + body.file)
      if (fs.existsSync(file)) {
        //  callback(null, profileImage)
        return resolve((file));
      } else {
        return resolve('nofile found');
      }
    });
  }
