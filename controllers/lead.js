'use strict';

const _ = require('underscore');
require('dotenv').config();
const User = require('./../models/user');
const Lead = require('./../models/lead');
const commonFunctions = require('./../commonFunctions.js');
const Referrals = require('./../models/referrals');
const async = require('async');
module.exports={
  saveLead,
  assignLead,
  editLead,
  editLeadStatus,
  leadDetail,
  removeLead,
  convertLeadByAgent,
  editLeadByAgent
};

function assignLead(body) {
  return new Promise(function(resolve, reject){
    var newBody = _.pick(body, 'leadId','agentId','mls');
    if(!newBody.hasOwnProperty('leadId') || !newBody.hasOwnProperty('agentId') || !newBody.hasOwnProperty('mls')
    || newBody.leadId === '' || newBody.agentId === '' || newBody.mls === '' || newBody.leadId.trim().length <= 0
    || newBody.agentId.trim().length <= 0 || newBody.mls.trim().length <= 0 ) {
      return reject(commonFunctions.sendErrorResponse('Missing parameters'));
    } else {
      var query = { '_id': newBody.leadId, 'properties.mls': newBody.mls };
      Lead.findOneAndUpdate( query, { $set: { 'properties.$.agentId': newBody.agentId } }, { new: true }, (err, updatedLead) => {
          if (err){
            return reject(commonFunctions.sendErrorResponse(err));
          }else{
            User.find({'role':'admin'}, function(err, res){
              if(err){
                return reject(commonFunctions.sendErrorResponse(err));
              }else{
                var message = {
                        to: updatedLead.email,
                        from: process.env.SENDGRID_API_EMAIL,
                        subject: 'Assign Lead',
                        text: 'test',
                        html: "<p>Dear " + updatedLead.firstName + ",</p><p>Admin assign new lead.</p>",
                      };
                commonFunctions.sendGridEmail(message).then(function(emailSent){
                  return resolve(commonFunctions.sendSuccessResponse(emailSent));
                }, function(error){
                  return reject(commonFunctions.sendErrorResponse(error));
                }).catch(function(e){
                  return reject(commonFunctions.sendErrorResponse(e));
                });
                var message1 = {
                        to: res[0].email,
                        from: process.env.SENDGRID_API_EMAIL,
                        subject: 'Assign Lead',
                        text: 'test',
                        html: "<p>Dear " + res[0].firstName + ",</p><p>Admin assign new lead.</p>",
                      };
                commonFunctions.sendGridEmail(message1).then(function(emailSent){
                  return resolve(commonFunctions.sendSuccessResponse(emailSent));
                }, function(error){
                  return reject(commonFunctions.sendErrorResponse(error));
                }).catch(function(e){
                  return reject(commonFunctions.sendErrorResponse(e));
                });
                return resolve(commonFunctions.sendSuccessResponse(updatedLead));
              }
            });
          }
      });
    }
  });
}

function editLead(body){
  return new Promise(function(resolve, reject){
    var newBody = _.pick(body, 'leadId','properties','firstName');
    if(!newBody.hasOwnProperty('leadId') || newBody.leadId === '' || newBody.leadId.trim().length <= 0
    || !newBody.hasOwnProperty('firstName') || newBody.firstName === '' || newBody.firstName.trim().length <= 0
    || !newBody.hasOwnProperty('properties')  || newBody.properties.length !== undefined ) {
      return reject(commonFunctions.sendErrorResponse('Missing parameters'));
    } else {
      User.find({ '_id': newBody.leadId }, (error, leadId) => {
        if(error){
            return reject(commonFunctions.sendErrorResponse('lead id not found'));
        } else {
              var query = { _id : newBody.leadId };
              Lead.findOneAndUpdate(query, { $set: body }, (err, updatedLead) => {
                if(err) {
                  return reject(commonFunctions.sendErrorResponse(err));
                }else{
                  return resolve(commonFunctions.sendSuccessResponse("Lead successfully update"));
                }
              });
          }
      }).select('_id');
    }
  });
}

function saveLead(body) {
  return new Promise(function(resolve, reject){
    var newBody = _.pick(body, 'firstName','email','properties');
    if(!newBody.hasOwnProperty('firstName') || !newBody.hasOwnProperty('email') || !newBody.hasOwnProperty('properties') || newBody.email === '' || newBody.firstName === ''
    || newBody.email.trim().length <= 0 || newBody.firstName.trim().length <= 0 || newBody.properties.length !== undefined ) {
      return reject(commonFunctions.sendErrorResponse('Invalid or missing parameters'));
    } else {
      var query = { email: newBody.email };
      Lead.findOne(query, (err, lead) => {
        if (err) {
          return reject(commonFunctions.sendErrorResponse(err));
        } else {
          if(lead){
          /*  autoAssignLead(function(res){
              return resolve(commonFunctions.sendSuccessResponse(res));
            }); */
            Lead.update( { 'email': newBody.email, 'properties.$.mls': { $ne: newBody.properties.mls } }, { $push: { properties: newBody.properties } }, (err, updatedLead) => {
              if(err) {
                return reject(commonFunctions.sendErrorResponse(err));
              } else {
                return resolve(commonFunctions.sendSuccessResponse("Lead successfully updated"));
              }
            });
          } else {
          /*  autoAssignLead(function(res){
              return resolve(commonFunctions.sendSuccessResponse(res));
            }); */
            var lead = new Lead(body);
             lead.save((err, newLead) => {
              if(err) {
                return reject(commonFunctions.sendErrorResponse(err));
              } else {
               User.find({'role':'admin'}, function(err, resp){
                  if(err){
                    return reject(commonFunctions.sendErrorResponse(err));
                  }else{
                    var message = {
                            to: newBody.email,
                            from: process.env.SENDGRID_API_EMAIL,
                            subject: 'Create New Lead',
                            text: 'test',
                            html: "<p>Dear " + newBody.firstName + ",</p><p>Admin create new lead.</p>",
                          };
                    commonFunctions.sendGridEmail(message).then(function(emailSent){
                      return resolve(commonFunctions.sendSuccessResponse(emailSent));
                    }, function(error){
                      return reject(commonFunctions.sendErrorResponse(error));
                    }).catch(function(e){
                      return reject(commonFunctions.sendErrorResponse(e));
                    });
                    var message1 = {
                            to: resp[0].email,
                            from: process.env.SENDGRID_API_EMAIL,
                            subject: 'Create New Lead',
                            text: 'test',
                            html: "<p>Dear " + resp[0].firstName + ",</p><p>Admin create new lead..</p>",
                          };
                    commonFunctions.sendGridEmail(message1).then(function(emailSent){
                      return resolve(commonFunctions.sendSuccessResponse(emailSent));
                    }, function(error){
                      return reject(commonFunctions.sendErrorResponse(error));
                    }).catch(function(e){
                      return reject(commonFunctions.sendErrorResponse(e));
                    });

                    return resolve(commonFunctions.sendSuccessResponse("Lead successfully created"));
                  }
                });
              }
            });
          }
        }
      });
    }
  });
}

function autoAssignLead(callback){
  var userZip = [];
  var leadZip = [];
  var finalZip =[];
  async.auto({
    getUserZipCode:[function(cb){
      User.find({ zip : { $exists: true } },{zip:1},{lean:true},function(err, res){
        if(err) return cb(commonFunctions.sendErrorResponse(err));
        if(res.length == 0)   return cb(commonFunctions.sendErrorResponse(err));
        var res1=   res.map(a => a.zip);
        userZip = res1;
        return cb(null,res);
      });
    }],
    getLeadZipCode:[function(cb){
      Lead.aggregate([{$unwind:"$properties"},{$match:{"properties.zip":{$exists:true}}},{$group:{_id:null, zipCode:{$addToSet:"$properties.zip"}}}], function(err, res){
        if(err) return cb(commonFunctions.sendErrorResponse(err));
        if(res.length == 0)   return cb(commonFunctions.sendErrorResponse(err));
        leadZip = res[0].zipCode
        return cb(null,res);
      })
    }],
    bindAllZipCode: ['getUserZipCode','getLeadZipCode',function(r1,OuterCb){
      finalZip = _.union(userZip,leadZip);
            async.eachSeries(finalZip, function(item, InnerOuterCb){
              var lat,long;
              async.auto({
                getGoogleResponse: [function(cb){
                  commonFunctions.googleLatLog(item).then(function(result){
                    var res = JSON.parse(result);
                    lat = res.success[0].latitude;
                    long = res.success[0].longitude;
                    // console.log('--- result --- ',item, res.success[0].latitude, res.success[0].longitude);
                    return cb(null, result);
                  });
                }],
                updateUser: ['getGoogleResponse',function(j1,cb){
                  var location = {
                    type:"Point",
                    coordinates:[long,lat],
                  }
                  var dataToSet={
                    $set :{ location:location }
                  }
                  User.update({'zip': item.toString() }, dataToSet, {multi:true}, function(err, resp){
                      if(err) return cb(err);
                      return cb(null, resp);
                  });
                }],
                updateLead: ['updateUser', function(j1,cb){
                  var location = {
                    type:"Point",
                    coordinates:[long,lat],
                  }
                  var dataToSet={
                    $set :{location:location}
                  }
                  Lead.aggregate([ {$unwind:"$properties"}, {$match:{"properties.zip":{$exists:true}}},
                         { $group : { _id:"$properties.zip", id: { $push : "$_id" } } } ], function(err, res){
                           if(err) return cb(err);
                      });
                }]
              },function(err,result){
                if(err) return InnerOuterCb(err);
                 return InnerOuterCb();
              })
              return  InnerOuterCb()
            },function(err,reuslt)
            {
              if(err) return OuterCb(err);
              return OuterCb();
            });
    }]
  },function(err,result){
    if(err) return callback(err)
    return callback(result.bindAllZipCode);
  })
}

function editLeadStatus(body){
  return new Promise(function(resolve, reject){
    var newBody = _.pick(body, 'leadId','status','agentId');
    if(!newBody.hasOwnProperty('leadId') || newBody.leadId === '' || newBody.leadId.trim().length <= 0
    || !newBody.hasOwnProperty('status') || newBody.status === '' || newBody.status.trim().length <= 0
    || !newBody.hasOwnProperty('agentId') || newBody.agentId === '' || newBody.agentId.trim().length <= 0) {
      return reject(commonFunctions.sendErrorResponse('Invalid or missing parameters'));
    } else {
      var query = { '_id': newBody.leadId };
      Lead.findOne(query, (err, res) => {
        if(err){
          return reject(commonFunctions.sendErrorResponse('lead id not found'));
        }else{
          if(res.length <= 0){
            return reject(commonFunctions.sendErrorResponse('no lead found'));
          }else{
            var query = {'_id': newBody.leadId,'properties.agentId': newBody.agentId};
            Lead.findOneAndUpdate(query, {$set:{'properties.status' : newBody.status}}, function(err, resp){
              if(err){
                return reject(commonFunctions.sendErrorResponse(err));
              }else{
                User.find({'role':'admin'}, function(err, res){
                  if(err){
                    return reject(commonFunctions.sendErrorResponse(err));
                  }else{
                    var adminEmail = res[0].email;
                    var message = {
                            to: resp.email,
                            from: process.env.SENDGRID_API_EMAIL,
                            subject: 'Update Lead Status',
                            text: 'test',
                            html: "<p>Dear " + resp.firstName + ",</p><p>Lead status change <b>"+newBody.status+"</b>.</p>",
                          };
                    commonFunctions.sendGridEmail(message).then(function(emailSent){
                      return resolve(commonFunctions.sendSuccessResponse(emailSent));
                    }, function(error){
                      return reject(commonFunctions.sendErrorResponse(error));
                    }).catch(function(e){
                      return reject(commonFunctions.sendErrorResponse(e));
                    });
                    var message1 = {
                            to: adminEmail,
                            from: process.env.SENDGRID_API_EMAIL,
                            subject: 'Update Lead Status',
                            text: 'test',
                            html: "<p>Dear " + res[0].firstName + ",</p><p>Lead status change <b>"+newBody.status+"</b>.</p>",
                          };
                    commonFunctions.sendGridEmail(message1).then(function(emailSent){
                      return resolve(commonFunctions.sendSuccessResponse(emailSent));
                    }, function(error){
                      return reject(commonFunctions.sendErrorResponse(error));
                    }).catch(function(e){
                      return reject(commonFunctions.sendErrorResponse(e));
                    });
                    return resolve(commonFunctions.sendSuccessResponse('lead status update'));
                  }
                });
              }
            });
          }
        }
      });
    }
  });
}

function leadDetail(body){
  return new Promise(function(resolve, reject){
    var newBody = _.pick(body, 'leadId');
    if(!newBody.hasOwnProperty('leadId') || newBody.leadId === '' || newBody.leadId.trim().length <= 0){
      return reject(commonFunctions.sendErrorResponse('Invalid or missing parameters'));
    }else{
      var query = { '_id': newBody.leadId };
      Lead.findOne(query, (err, res) => {
        if(err){
          return reject(commonFunctions.sendErrorResponse(err));
        }else{
          return resolve(commonFunctions.sendSuccessResponse(res));
        }
      });
    }
  });
}

function removeLead(body){
  return new Promise(function(resolve, reject){
    var newBody = _.pick(body, 'leadId');
    if(!newBody.hasOwnProperty('leadId') || newBody.leadId === '' || newBody.leadId.trim().length <= 0){
      return reject(commonFunctions.sendErrorResponse('Invalid or missing parameters'));
    }else{
      Lead.remove({_id: newBody.leadId}, (err, res) => {
        if(err){
          return reject(commonFunctions.sendErrorResponse(err));
        }else{
          Lead.find({},(err, res) => {
            if(err){
              return reject(commonFunctions.sendErrorResponse(err));
            }else{
              return resolve(commonFunctions.sendSuccessResponse(res));
            }
          });
        }
      });
    }
  });
}

function convertLeadByAgent(body){
  return new Promise(function(resolve, reject){
    var newBody = _.pick(body, 'leadId');
    if(!newBody.hasOwnProperty('leadId') || newBody.leadId === '' || newBody.leadId.trim().length <= 0){
      return reject(commonFunctions.sendErrorResponse('Invalid or missing parameters'));
    }else{
      Lead.find({'_id': newBody.leadId},{},{lean:true},(err, res) => {
        if(err){
          return reject(commonFunctions.sendErrorResponse(err));
        }else{
            var properties1 = res[0].properties;
            var properties =[];
            properties1.forEach(function (value, index) {
                  var obj = value;
                  delete obj._id;
                  obj['receivingAgentId'] = [body.receivingAgentId[0]];
                  properties.push(obj);
            });
         var refData = {
            "firstName": res[0].firstName,
            "lastName" : res[0].lastName,
            "email" : res[0].email,
            "company" : res[0].company,
            "source" : res[0].source,
            "message" : res[0].message,
            "refAddress" : res[0].leadAddress,
            "referralStatus" : "Pending",
            properties
          };
          var referrals = new Referrals(refData);
          referrals.save((err, newRefrer) => {
            if (err) {
              return reject(commonFunctions.sendErrorResponse(err));
            } else {
              Lead.findOneAndUpdate({'_id': newBody.leadId},{$set: {'status':'0'}},{ new: true },(err, res) => {
                if(err){
                  return reject(commonFunctions.sendErrorResponse(err));
                }
              });
              return resolve(commonFunctions.sendSuccessResponse("Your lead successfully convert to refferal"));
            }
          });
        }
      });
    }
  });
}
function editLeadByAgent(body){
  return new Promise(function(resolve, reject){
    var newBody = _.pick(body, 'email');
    if(!newBody.hasOwnProperty('email') || newBody.email === '' || newBody.email.trim().length <= 0){
      return reject(commonFunctions.sendErrorResponse('Invalid or missing parameters'));
    }else{
      Lead.findOneAndUpdate({'email': newBody.email},{$set: body},{ new: true },(err, res) => {
        if(err){
          return reject(commonFunctions.sendErrorResponse(err));
        }else{
          return resolve(commonFunctions.sendSuccessResponse("Lead successfully updated"));
        }
      });
    }
  });
}
