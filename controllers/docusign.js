'use strict';

const _ = require('underscore');
const Docusign = require('./../models/docusign');
const User = require('./../models/user');
const ManageContent = require('./../models/manageContent');
require('dotenv').config();
var formidable = require('formidable');
const commonFunctions = require('./../commonFunctions.js');

module.exports={
  integration,
  showAllAgreements,
  manageContent,
  editManageContent,
  removeManageContent,
  getManageContent
};

function integration(body){
  return new Promise(function(resolve, reject){
    var newBody = _.pick(body, 'ownerId','email','password','apiKey');
    if( !newBody.hasOwnProperty('ownerId') || newBody.ownerId === '' || newBody.ownerId.trim().length <= 0
        || !newBody.hasOwnProperty('email') || newBody.email === '' || newBody.email.trim().length <= 0
        || !newBody.hasOwnProperty('password') || newBody.password === '' || newBody.password.trim().length <= 0
        || !newBody.hasOwnProperty('apiKey') || newBody.apiKey === '' || newBody.apiKey.trim().length <= 0 ) {
      return reject(commonFunctions.sendErrorResponse('Missing parameters here'));
    } else {
      User.find({$and: [{_id: newBody.ownerId,'role':'admin'}]}, (err, res) => {
        if(err){
          return reject(commonFunctions.sendErrorResponse('Admin id not found'));
        }else{
          if(res.length <= 0){
            return reject(commonFunctions.sendErrorResponse('Not admin id'));
          }else{
            commonFunctions.docusignCredentials(body).then(function(response){
              var query = { 'ownerId': newBody.ownerId };
              Docusign.findOneAndUpdate(query, { $set: newBody }, function(err, res) {
                if (err){
                  return reject(commonFunctions.sendErrorResponse(''));
                }else{
                  const docusign = new Docusign(newBody);
                  docusign.save(function(err, inserted) {
                    if (err){
                       return resolve(commonFunctions.sendSuccessResponse('Docusign credentials successfully update'));
                    }else{
                      if(inserted){
                        return resolve(commonFunctions.sendSuccessResponse('Docusign credentials successfully add'));
                      }
                    }
                  });
                }
              });
            }).catch(function(e){
              return reject(commonFunctions.sendErrorResponse(e));
            });
          }
        }
      });
    }
  });
}

function showAllAgreements(body){
  return new Promise(function(resolve, reject){
    var newBody = _.pick(body,'userId', 'pageNo');
    if( !newBody.hasOwnProperty('userId') || newBody.userId === '' || newBody.userId.trim().length <=0
        || !newBody.hasOwnProperty('pageNo') || newBody.pageNo === '' || newBody.pageNo.trim().length <=0 ){
      return reject(commonFunctions.sendErrorResponse('Missing parameters here'));
    }else{
      User.find( { $and:[{ _id: newBody.userId, 'role': 'admin' }] }, (err, res)=>{
        if(err){
          return reject(commonFunctions.sendErrorResponse('Admin id not found'));
        }else{
            if(res.length <= 0){
              return reject(commonFunctions.sendErrorResponse('Not admin id'));
            }else{
              if(newBody.pageNo == 0){
                newBody.pageNo = 0;
              }else{
                newBody.pageNo *= 50;
              }
              // page no start fron 0
              Docusign.find( { 'ownerId': newBody.userId }, function(error, response){
                commonFunctions.docusignAllAgreements(response[0], newBody.pageNo).then(function(response){
                  var resp = JSON.parse(response);
                  return resolve(commonFunctions.sendSuccessResponse(resp));
                }).catch(function(e){
                  return reject(commonFunctions.sendErrorResponse(e));
                });
              });
            }
        }
      });
    }
  });
}

function manageContent(body){
  return new Promise(function(resolve, reject){
    var newBody = _.pick(body,'name', 'description');
    if( !newBody.hasOwnProperty('name') || newBody.name === '' || newBody.name.trim().length <=0
        || !newBody.hasOwnProperty('description') || newBody.description === '' || newBody.description.trim().length <=0 ){
      return reject(commonFunctions.sendErrorResponse('Missing parameters here'));
    }else{
      var manageContent = new ManageContent(body);
      manageContent.save((err, res) => {
        if(err){
          return reject(commonFunctions.sendErrorResponse(err));
        }else{
          return resolve(commonFunctions.sendSuccessResponse(res));
        }
      });
    }
  });
}

function editManageContent(body){
  return new Promise(function(resolve, reject){
    var newBody = _.pick(body,'id', 'name', 'description');
    if( !newBody.hasOwnProperty('name') || newBody.name === '' || newBody.name.trim().length <=0
        || !newBody.hasOwnProperty('description') || newBody.description === '' || newBody.description.trim().length <=0
      || !newBody.hasOwnProperty('id') || newBody.id === '' || newBody.id.trim().length <=0){
      return reject(commonFunctions.sendErrorResponse('Missing parameters here'));
    }else{
      ManageContent.findOneAndUpdate({ '_id': newBody.id }, { $set: newBody }, function(err, res){
        if(err){
          return reject(commonFunctions.sendErrorResponse(err));
        }else{
          return resolve(commonFunctions.sendSuccessResponse('Content successfully update.'));
        }
      });
    }
  });
}

function removeManageContent(body){
  return new Promise(function(resolve, reject){
    var newBody = _.pick(body,'id');
    if( !newBody.hasOwnProperty('id') || newBody.id === '' || newBody.id.trim().length <=0){
      return reject(commonFunctions.sendErrorResponse('Missing parameters here'));
    }else{
      ManageContent.find( { '_id': newBody.id }, function(err, res){
        if(err){
          return reject(commonFunctions.sendErrorResponse('Content id not found'));
        }else{
          ManageContent.remove({ '_id': newBody.id }, function(err, res){
            if(err)  {
              return reject(commonFunctions.sendErrorResponse(err));
            }else{
              return resolve(commonFunctions.sendSuccessResponse('Content successfully removed.'));
            }
          });
        }
      });
    }
  });
}

function getManageContent(body){
  return new Promise(function(resolve, reject){
    var newBody = _.pick(body,'name');
    if( !newBody.hasOwnProperty('name') || newBody.name === '' || newBody.name.trim().length <=0){
      return reject(commonFunctions.sendErrorResponse('Missing parameters here'));
    }else{
      ManageContent.find( { 'name': newBody.name }, function(err, res){
        if(err){
          return reject(commonFunctions.sendErrorResponse(err));
        }else{
          if(res.length <= 0){
            return reject(commonFunctions.sendErrorResponse('Content name not found'));
          }else{
            console.log(res);
            return resolve(commonFunctions.sendSuccessResponse(res));
          }
        }
      });
    }
  });
}
