'use strict';

const _ = require('underscore');
require('dotenv').config();
const User = require('./../models/user');
const Lead = require('./../models/lead');
const commonFunctions = require('./../commonFunctions.js');
const Company = require('./../models/company');
const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;
var randtoken = require('random-token');
require('dotenv').config();
var formidable = require('formidable');
var BUCKET_NAME = 'citynative';
var aws = require('aws-sdk');

var configParamsS3 = {accessKeyId:process.env.ACCESS_KEY_ID,secretAccessKey: process.env.SECRET_ACCESS_KEY,region: process.env.REGION};

const jwt = require('jsonwebtoken');
var path= require('path');
var fs = require('file-system');

module.exports={
  getAllLeads,
  getLeadsByAgent,
  getLeadsByAgentStatus,
  createCompany,
  editCompany,
  deleteCompany,
  editProfile,
  getEnvelope,
  companyDetail,
  socialLinks,
  getLeadsByAdmin
};

function getAllLeads(body){
  return new Promise(function(resolve, reject){
    Lead.find({},(err, res) => {
      if(err){
          return reject(commonFunctions.sendErrorResponse(err));
      }else{
        return resolve(commonFunctions.sendSuccessResponse(res));
      }
    });
  });
}

function getLeadsByAgent(body) {
  return new Promise(function(resolve, reject){
    var newBody = _.pick(body, 'agentId');
    if( !newBody.hasOwnProperty('agentId') || newBody.agentId === '' || newBody.agentId.trim().length <= 0 ) {
      return reject(commonFunctions.sendErrorResponse('Missing parameters here'));
    } else {
      Lead.find({ 'properties.agentId': newBody.agentId },function(err,res){
        if(err){
            return reject(commonFunctions.sendErrorResponse(err));
        }else{
          if(res.length <= 0){
               return reject(commonFunctions.sendErrorResponse('Agent id not valid'));
          }else{
            Lead.find({ 'properties.agentId': newBody.agentId , 'status' : '1'}, function(err,result){
              if(err){
                return reject(commonFunctions.sendErrorResponse(err));
              }else{
                if(result.length<=0){
                    return reject(commonFunctions.sendErrorResponse('No record found'));
                } else{
                   return resolve(commonFunctions.sendSuccessResponse(result));
                }
              }
            });
          }
        }
      }).select('agentId');
    }
  });
}

function companyDetail(body){
  return new Promise(function(resolve, reject){
    var newBody = _.pick(body, 'agentId');
    if( !newBody.hasOwnProperty('agentId') || newBody.agentId === '' || newBody.agentId.trim().length <= 0){
      return reject(commonFunctions.sendErrorResponse('Missing Parameters'));
    }else{
      Company.find({agentId: newBody.agentId}, (err,res) => {
        if(err){
          return reject(commonFunctions.sendErrorResponse(err));
        }else{
          return resolve(commonFunctions.sendSuccessResponse(res));
        }
      });
    }
  });
}

function createCompany(body){
    return new Promise(function(resolve, reject){
      var newBody = _.pick(body, 'agentId','companyName','phoneNumber');
      if( !newBody.hasOwnProperty('agentId') || newBody.agentId === '' || newBody.agentId.trim().length <= 0
        || !newBody.hasOwnProperty('companyName') || newBody.companyName === '' || newBody.companyName.trim().length <= 0
        || !newBody.hasOwnProperty('phoneNumber')
        || newBody.phoneNumber === '' || newBody.phoneNumber.trim().length <= 0 ){
          return reject(commonFunctions.sendErrorResponse('Missing Parameters'));
      } else {
        User.find({_id: newBody.agentId}, (error, agent) => {
          if(error){
            return reject(commonFunctions.sendErrorResponse('agent id not found'));
          }else{
            Company.find({'agentId': body.agentId},(err, res)=>{
              if(err){
                return reject(commonFunctions.sendErrorResponse(err));
              }else{
                if(res.length > 0){
                  Company.findOneAndUpdate({'agentId': body.agentId}, { $set: body }, (err, res)=>{
                    if(err){
                      return reject(commonFunctions.sendErrorResponse(err));
                    }else{
                      return resolve(commonFunctions.sendSuccessResponse('Company successfully updated'));
                    }
                  });
                }else{
                  const company = new Company(body);
                  company.save((err, newCompany) => {
                    if(err) {
                        return reject(commonFunctions.sendErrorResponse(err));
                    } else {
                        return resolve(commonFunctions.sendSuccessResponse('New company successfully created '));
                    }
                  });
                }
              }
            });
          }
        });
      }
    });
  }

  function getLeadsByAdmin(body){
    return new Promise(function(resolve, reject){
      var newBody = _.pick(body, 'agentId','status');
      if( !newBody.hasOwnProperty('agentId') || newBody.agentId === '' || newBody.agentId.trim().length <= 0
      || !newBody.hasOwnProperty('status') || newBody.status === '' || newBody.status.trim().length <= 0 ) {
        return reject(commonFunctions.sendErrorResponse('Missing parameters here'));
      } else {
        Lead.find({ 'leadStatus': newBody.status }, function(err,result){
          if(err){
            return reject(commonFunctions.sendErrorResponse(err));
          }else{
            return resolve(commonFunctions.sendSuccessResponse(result));
          }
        });
        }
    });
  }

function getLeadsByAgentStatus(body){
  return new Promise(function(resolve, reject){
    var newBody = _.pick(body, 'agentId','status');
    if( !newBody.hasOwnProperty('agentId') || newBody.agentId === '' || newBody.agentId.trim().length <= 0
    || !newBody.hasOwnProperty('status') || newBody.status === '' || newBody.status.trim().length <= 0 ) {
      return reject(commonFunctions.sendErrorResponse('Missing parameters here'));
    } else {
      Lead.find({
        'properties.agentId': newBody.agentId
      }, function(err, res) {
        if (err) {
          return reject(commonFunctions.sendErrorResponse(err));
        } else {
          if (res.length <= 0) {
            return reject(commonFunctions.sendErrorResponse('Agent id not valid'));
          } else {
            var dat = {
              $and: [{
                  'properties.agentId': newBody.agentId
                },
                {
                  'status': '1'
                },
                {
                  'leadStatus' : newBody.status
                }
              ]
            };
            Lead.find(dat, function(err, result) {
              if (err) {
                return reject(commonFunctions.sendErrorResponse(err));
              } else {
                if (result.length <= 0) {
                  return reject(commonFunctions.sendErrorResponse('No record found'));
                } else {
                  return resolve(commonFunctions.sendSuccessResponse(result));
                }
              }
            }).select('firstName lastName email phone address message source modifyDate leadStatus company');
          }
        }
      }).select('agentId');
      }
  });
}


function editCompany(body){
  return new Promise(function(resolve, reject){
    var form = new formidable.IncomingForm();
    form.parse(body, function(err, fields, files) {
      form.multiples = true;
      form.uploadDir = path.join(__dirname, './../public/tmp');
      var token = fields.token;
      if(token === '' || token.trim().length <= 0) {
        reject(commonFunctions.sendErrorResponse('Missing parameters'));
      } else {
        jwt.verify(token, process.env.TOKEN_SECRET, function(err, decoded) {
         if(err) {
           reject(commonFunctions.sendErrorResponse(err));
         } else {
           if( !fields.hasOwnProperty('agentId') || fields.agentId === '' || fields.agentId.trim().length <= 0
              || !fields.hasOwnProperty('companyName') || fields.companyName === '' || fields.companyName.trim().length <= 0
              || !fields.hasOwnProperty('brokerageName') || fields.brokerageName === '' || fields.brokerageName.trim().length <= 0 || !fields.hasOwnProperty('licenseExpireDate') || fields.licenseExpireDate === '' || fields.licenseExpireDate.trim().length <= 0
              || !fields.hasOwnProperty('phoneNumber') || fields.phoneNumber === '' || fields.phoneNumber.trim().length <= 0
              || !fields.hasOwnProperty('fax') || fields.fax === '' || fields.fax.trim().length <= 0
              || !fields.hasOwnProperty('address') || fields.address === '' || fields.address.trim().length <= 0
              || !fields.hasOwnProperty('city') || fields.city === '' || fields.city.trim().length <= 0
              || !fields.hasOwnProperty('state') || fields.state === '' || fields.state.trim().length <= 0
              || !fields.hasOwnProperty('zip') || fields.zip === '' || fields.zip.trim().length <= 0) {
                return reject(commonFunctions.sendErrorResponse('Missing parameters'));
              } else {
                Company.find({'agentId': fields.agentId}, (error, agent) => {
                  if(error) {
                    return reject(commonFunctions.sendErrorResponse(error));
                  } else {
                    if(agent.length <= 0){
                      return reject(commonFunctions.sendErrorResponse('agent id not found'));
                    }else{
                    // update company
                      var query = { 'agentId' : fields.agentId };
                      Company.findOneAndUpdate(query, { $set: fields }, (err, updatedCompany) => {
                        if (err) return reject(commonFunctions.sendErrorResponse(err));
                        return resolve(commonFunctions.sendSuccessResponse('company updated'));
                      });

                    var fileType = files.companyLogo.type;
                      var fileType = fileType.split('/').pop();
                      if (fileType !== 'png' && fileType !== 'jpeg' && !fileType !== 'jpg') {
                          return reject(commonFunctions.sendErrorResponse('Invalid file type'));
                      }
                      var imageNameToSave = randtoken(32) + '.' + fileType;
                      fs.rename(files.companyLogo.path, path.join(form.uploadDir, imageNameToSave));
                        var s3 = new aws.S3(configParamsS3);
                      var fileName = __dirname + '/../public/tmp/' + imageNameToSave;
                      var fileBuffer = fs.readFileSync(fileName);
                      var metaData = 'image/jpg';
                      var saveToDb = 'https://s3-' + process.env.REGION + '.amazonaws.com/' + process.env.BUCKET_NAME + '/' + imageNameToSave;
                     s3.putObject({
                          ACL: 'public-read',
                          Bucket: BUCKET_NAME,
                          Key: imageNameToSave,
                          Body: fileBuffer,
                          ContentType: metaData
                      }, function(error, response) {
                          if (error) {
                              return reject(commonFunctions.sendErrorResponse(error));
                          } else {
                            var q = { 'agentId' : fields.agentId };
                            Company.findOneAndUpdate(q, { $set: {'companyLogo' : saveToDb} }, (err, updatedCompany) => {
                              if(err){
                                return reject(commonFunctions.sendErrorResponse(err));
                              }else {
                                fs.unlink(fileName);
                                return resolve(commonFunctions.sendSuccessResponse('Comany updated'));
                              }
                            });
                          }
                      });
                    }
                  }
                });
              }
            }
         });
      }
    });
  });
}

function deleteCompany(body){
  return new Promise(function(resolve, reject){
    var newBody = _.pick(body, 'agentId','companyId');
    if(!newBody.hasOwnProperty('agentId') || newBody.agentId === '' || newBody.agentId.trim().length <= 0
      || !newBody.hasOwnProperty('companyId') || newBody.companyId === '' || newBody.companyId.trim().length <= 0 ) {
      return reject(commonFunctions.sendErrorResponse('Missing Parameters'));
    } else {
        Company.find({'agentId': newBody.agentId}, (error, agent) => {
          if(error) {
            return reject(commonFunctions.sendErrorResponse(error));
          } else {
            if(agent.length <= 0){
              return reject(commonFunctions.sendErrorResponse('agent id not found'));
            }else{
              Company.remove({_id: newBody.companyId}, (error, agent) => {
                if(error) {
                  return reject(commonFunctions.sendErrorResponse(error));
                } else {
                  return resolve(commonFunctions.sendSuccessResponse('Your comany successfully removed'));
                }
              });
            }
          }
        });
      }
  });
}
function getEnvelope(body){
  return new Promise(function(resolve, reject){
    var newBody = _.pick(body, 'agentId','envelopeId');
    if(!newBody.hasOwnProperty('agentId') || newBody.agentId === '' || newBody.agentId.trim().length <= 0
        || !newBody.hasOwnProperty('envelopeId') || newBody.envelopeId === '' || newBody.envelopeId.trim().length <= 0){
      return reject(commonFunctions.sendErrorResponse('Missing parameters'));
    }else{
      User.findOne({ '_id':newBody.agentId }, function(err,res){
        if(err){
          return reject(commonFunctions.sendErrorResponse('agent id not found'));
        }else{
          if(res.length <= 0){
            return reject(commonFunctions.sendErrorResponse('agent id not found'));
          }else{
            User.findOne({ 'envelopeId' : newBody.envelopeId }, function(err,res){
              if(err){
                return reject(commonFunctions.sendErrorResponse('envelope id not found'));
              }else{
                  commonFunctions.docusignGetEnvelope(res.envelopeId).then(function(envelopeRes){
                    return resolve(envelopeRes);
                  }).catch(function(e){
                    return reject(commonFunctions.sendErrorResponse(e));
                  });
                  //return reject(commonFunctions.sendErrorResponse('envelope id not found'));
              }
            });

          }
        }
      });
    }
  });
}
function editProfile(body){
  return new Promise(function(resolve, reject){
    var newBody = _.pick(body, 'agentId');
    if(!newBody.hasOwnProperty('agentId') || newBody.agentId === '' || newBody.agentId.trim().length <= 0){
      return reject(commonFunctions.sendErrorResponse('Missing parameters'));
    }else{
      User.findOneAndUpdate({'_id':newBody.agentId},{ $set: body }, (err, res) => {
        if(err){
          return reject(commonFunctions.sendErrorResponse(err));
        }else{
          return resolve(commonFunctions.sendSuccessResponse('Your profile successfully updated'));
        }
      });
    }
  });
}

function socialLinks(body){
  console.log('body', body);
  return new Promise(function(resolve, reject){
    var newBody = _.pick(body, 'agentId');
    if(!newBody.hasOwnProperty('agentId') || newBody.agentId === '' || newBody.agentId.trim().length <= 0){
      return reject(commonFunctions.sendErrorResponse('Missing parameters'));
    }else{
      User.findOneAndUpdate({'_id':newBody.agentId},{ $set: { 'socialLinks' : {'facebook': body.facebook, 'twitter': body.twitter} } }, (err, res) => {
        if(err){
          return reject(commonFunctions.sendErrorResponse(err));
        }else{
          return resolve(commonFunctions.sendSuccessResponse('Your social links successfully updated'));
        }
      });
    }
  });
}

/*function editProfile(body){
  console.log('body', body);
  return new Promise(function(resolve, reject){
    var form = new formidable.IncomingForm();
    form.parse(body, function(err, fields, files) {
      console.log('err', err);
      console.log('fields', fields);
      console.log('files', files);
      form.multiples = true;
      form.uploadDir = path.join(__dirname, './../public/tmp');
      var token = fields.token;
      if(token === '' || token.trim().length <= 0) {
        reject(commonFunctions.sendErrorResponse('Missing parameters'));
      } else {
        jwt.verify(token, process.env.TOKEN_SECRET, function(err, decoded) {
          if(err) {
            reject(commonFunctions.sendErrorResponse(err));
          } else {
             if( !fields.hasOwnProperty('agentId') || fields.agentId === '' || fields.agentId.trim().length <= 0
              || !fields.hasOwnProperty('email') || fields.email === '' || fields.email.trim().length <= 0
              || !fields.hasOwnProperty('firstName') || fields.firstName === '' || fields.firstName.trim().length <= 0
              || !fields.hasOwnProperty('lastName') || fields.lastName === '' || fields.lastName.trim().length <= 0) {
                return reject(commonFunctions.sendErrorResponse('Missing parameters'));
              } else {
                User.find({_id: fields.agentId}, (error, agent) => {
                  if(error) {
                    return reject(commonFunctions.sendErrorResponse(error));
                  } else {
                    var query = { _id : fields.agentId };
                    User.findOneAndUpdate(query, { $set: fields }, (err, updatedAgent) => {
                      if (err) return reject(commonFunctions.sendErrorResponse(err));
                      return resolve(commonFunctions.sendSuccessResponse('Agent updated'));
                    });
                      var fileType = files.profileImage.type;
                      var fileType = fileType.split('/').pop();
                      if (fileType !== 'png' && fileType !== 'jpeg' && !fileType !== 'jpg') {
                          return reject(commonFunctions.sendErrorResponse('Invalid file type'));
                      }
                      var imageNameToSave = randtoken(32) + '.' + fileType;
                      fs.rename(files.profileImage.path, path.join(form.uploadDir, imageNameToSave));
                      var s3 = new aws.S3(configParamsS3);
                      var fileName = __dirname + '/../public/tmp/' + imageNameToSave;
                      var fileBuffer = fs.readFileSync(fileName);
                      var metaData = 'image/jpg';
                      var saveToDb = 'https://s3-' + process.env.REGION + '.amazonaws.com/' + process.env.BUCKET_NAME + '/' + imageNameToSave;
                      s3.putObject({
                          ACL: 'public-read',
                          Bucket: BUCKET_NAME,
                          Key: imageNameToSave,
                          Body: fileBuffer,
                          ContentType: metaData
                      }, function(error, response) {
                          if (error) {
                              return reject(commonFunctions.sendErrorResponse(error));
                          } else {
                            User.findOneAndUpdate(query, { $set: {'profileImage' : saveToDb} }, (err, updatedAgent) => {
                              if(err){
                                return reject(commonFunctions.sendErrorResponse(err));
                              }else {
                                fs.unlink(fileName);
                                return resolve(commonFunctions.sendSuccessResponse('Agent updated successfully'));
                              }
                            });
                          }
                      });
                  }
                });
              }
          }
        });
      }
    });
 });
} */
