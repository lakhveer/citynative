'use strict';

const _ = require('underscore');
const User = require('./../models/user');
const Company = require('./../models/company');
const Docusign = require('./../models/docusign');
const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;
const commonFunctions = require('./../commonFunctions.js');
var randtoken = require('random-token');
require('dotenv').config();
var formidable = require('formidable');
var BUCKET_NAME = 'citynative';
var aws = require('aws-sdk');
var exec = require('child_process').exec;

var configParamsS3 = {
  accessKeyId: process.env.ACCESS_KEY_ID,
  secretAccessKey: process.env.SECRET_ACCESS_KEY,
  region: process.env.REGION
};

const jwt = require('jsonwebtoken');
var path = require('path');
var fs = require('file-system');

module.exports = {
  createUser,
  login,
  showAllAgents,
  agentDetails,
  agentEditDetails,
  updateAgent,
  agentRemove,
  changePassword,
  resetPassword,
  resetPasswordChange,
  signup,
  verifyUserEmail,
  docusign,
  docusigndownloadDocument,
  changeAgentPassword
};

function createUser(body) {
  return new Promise(function(resolve, reject) {
    var newBody = _.pick(body, 'email', 'firstName', 'lastName', 'role', 'password');
    if (Object.keys(newBody).length > 0) {
      if (!newBody.hasOwnProperty('email') || !newBody.hasOwnProperty('firstName') || !newBody.hasOwnProperty('lastName') ||
        !newBody.hasOwnProperty('role') || newBody.email === '' || newBody.firstName === '' || newBody.lastName === '' || newBody.role === '' || newBody.email.trim().length <= 0 ||
        newBody.role.trim().length <= 0 || newBody.firstName.trim().length <= 0 || newBody.lastName.trim().length <= 0
      ) {
        return reject(commonFunctions.sendErrorResponse('Missing Parameters'));
      } else {
        User.findOne({
          email: newBody.email
        }, (error, result) => {
          if (result) {
            return reject(commonFunctions.sendErrorResponse('User exists'));
          } else {
            newBody.password = newBody.password;
            const newUser = new User(newBody);
            newUser.save(function(err, inserted) {
              if (err) return reject(commonFunctions.sendErrorResponse(err));
              if (inserted) {
                return resolve(commonFunctions.sendSuccessResponse('User Inserted'));
              }
            });
          }
        });
      }
    } else {
      return reject(commonFunctions.sendErrorResponse('Missing Parameters'));
    }
  });
}

function login(body) {
  return new Promise(function(resolve, reject) {
    var newBody = _.pick(body, 'email', 'password');
    if (Object.keys(newBody).length > 0) {
      if (!newBody.hasOwnProperty('email') || !newBody.hasOwnProperty('password') || newBody.email === '' || newBody.password === '' || newBody.email.trim().length <= 0 || newBody.password.trim().length <= 0) {
        reject(commonFunctions.sendErrorResponse('Missing Parameters'));
      } else {
        User.findOne({
          email: newBody.email
        }, (error, foundUser) => {
          if (error) {
            return reject(commonFunctions.sendErrorResponse(error));
          } else {
            if (foundUser) {
              bcrypt.compare(newBody.password, foundUser.password, function(err, response) {
                if (err) {
                  return reject(commonFunctions.sendErrorResponse('Error in verifying user'));
                } else {
                  if (response === false) {
                    return reject(commonFunctions.sendErrorResponse('Invalid username or password'));
                  } else {
                    commonFunctions.createToken(foundUser).then(function(tokenResponse) {
                      return resolve(commonFunctions.sendSuccessResponse(tokenResponse));
                    }, function(error) {
                      return reject(commonFunctions.sendErrorResponse(error));
                    }).catch(function(e) {
                      return reject(commonFunctions.sendErrorResponse(e));
                    });
                  }
                }
              });
            } else {
              return reject(commonFunctions.sendErrorResponse('User not found'));
            }
          }
        });
      }
    } else {
      return reject(commonFunctions.sendErrorResponse('Missing Parameters'));
    }
  });
}

function showAllAgents() {
  return new Promise(function(resolve, reject) {
    User.find({
      role: 'agent'
    }, (error, foundUsers) => {
      if (error) {
        return reject(commonFunctions.sendErrorResponse(error));
      } else {
        if (foundUsers) {
          return resolve(commonFunctions.sendSuccessResponse(foundUsers));
        } else {
          return reject(commonFunctions.sendErrorResponse('No agents found'));
        }
      }
    });
  });
}

function agentDetails(body) {
  return new Promise(function(resolve, reject) {
    var newBody = _.pick(body, 'agentId');
    if (!newBody.hasOwnProperty('agentId') || newBody.agentId === '' || newBody.agentId.trim().length <= 0) {
      return reject(commonFunctions.sendErrorResponse('Missing parameters'));
    } else {
      User.find({
        _id: newBody.agentId
      }, (error, agent) => {
        if (error) {
          return reject(commonFunctions.sendErrorResponse(error));
        } else {
          return resolve(commonFunctions.sendSuccessResponse(agent));
        }
      });
    }
  });
}

function agentEditDetails(body) {
  return new Promise(function(resolve, reject) {
    var form = new formidable.IncomingForm();
    form.parse(body, function(err, fields, files) {
      form.multiples = true;
      form.uploadDir = path.join(__dirname, './../public/tmp');
      var token = fields.token;
      if (token === '' || token.trim().length <= 0) {
        reject(commonFunctions.sendErrorResponse('Missing parameters'));
      } else {
        jwt.verify(token, process.env.TOKEN_SECRET, function(err, decoded) {
          if (err) {
            reject(commonFunctions.sendErrorResponse(err));
          } else {
            if (!fields.hasOwnProperty('agentId') || fields.agentId === '' || fields.agentId.trim().length <= 0 ||
              !fields.hasOwnProperty('email') || fields.email === '' || fields.email.trim().length <= 0 ||
              !fields.hasOwnProperty('firstName') || fields.firstName === '' || fields.firstName.trim().length <= 0 ||
              !fields.hasOwnProperty('lastName') || fields.lastName === '' || fields.lastName.trim().length <= 0
            ) {
              return reject(commonFunctions.sendErrorResponse('Missing parameters'));
            } else {
              User.find({
                _id: fields.agentId
              }, (error, agent) => {
                if (error) {
                  return reject(commonFunctions.sendErrorResponse(error));
                } else {
                  var query = {
                    _id: fields.agentId
                  };
                  User.findOneAndUpdate(query, {
                    $set: fields
                  }, (err, updatedAgent) => {
                    if (err) return reject(commonFunctions.sendErrorResponse(err));
                    return resolve(commonFunctions.sendSuccessResponse('Agent updated'));
                  });
                  var fileType = files.profileImage.type;
                  var fileType = fileType.split('/').pop();
                  if (fileType !== 'png' && fileType !== 'jpeg' && !fileType !== 'jpg') {
                    return reject(commonFunctions.sendErrorResponse('Invalid file type'));
                  }
                  var imageNameToSave = randtoken(32) + '.' + fileType;
                  fs.rename(files.profileImage.path, path.join(form.uploadDir, imageNameToSave));
                  var s3 = new aws.S3(configParamsS3);
                  var fileName = __dirname + '/../public/tmp/' + imageNameToSave;
                  var fileBuffer = fs.readFileSync(fileName);
                  var metaData = 'image/jpg';
                  var saveToDb = 'https://s3-' + process.env.REGION + '.amazonaws.com/' + process.env.BUCKET_NAME + '/' + imageNameToSave;
                  s3.putObject({
                    ACL: 'public-read',
                    Bucket: BUCKET_NAME,
                    Key: imageNameToSave,
                    Body: fileBuffer,
                    ContentType: metaData
                  }, function(error, response) {
                    if (error) {
                      return reject(commonFunctions.sendErrorResponse(error));
                    } else {
                      User.findOneAndUpdate(query, {
                        $set: {
                          'profileImage': saveToDb
                        }
                      }, (err, updatedAgent) => {
                        if (err) {
                          return reject(commonFunctions.sendErrorResponse(err));
                        } else {
                          fs.unlink(fileName);
                          return resolve(commonFunctions.sendSuccessResponse('Agent updated'));
                        }
                      });
                    }
                  });
                }
              });
            }
          }
        });
      }
    });
  });
}

function updateAgent(body) {
  return new Promise(function(resolve, reject) {
    var newBody = _.pick(body, 'agentId');
    if (!newBody.hasOwnProperty('agentId') || newBody.agentId === '' || newBody.agentId.trim().length <= 0) {
      return reject(commonFunctions.sendErrorResponse('Missing parameters'));
    } else {
      var query = {
        _id: newBody.agentId
      };
      User.findOneAndUpdate(query, {
        $set: body
      }, {
        new: true
      }, function(err, agent) {
        if (err) {
          return reject(commonFunctions.sendErrorResponse(err));
        } else {
          return resolve(commonFunctions.sendSuccessResponse(agent));
        }
      });
    }
  });
}

function agentRemove(body) {
  return new Promise(function(resolve, reject) {
    var newBody = _.pick(body, 'agentId');
    if (!newBody.hasOwnProperty('agentId') || newBody.agentId === '' || newBody.agentId.trim().length <= 0) {
      return reject(commonFunctions.sendErrorResponse('Missing parameters'));
    } else {
      User.remove({
        _id: newBody.agentId
      }, (err, res) => {
        if (err) {
          return reject(commonFunctions.sendErrorResponse(err));
        } else {
          User.find({}, (err, res) => {
            if (err) {
              return reject(commonFunctions.sendErrorResponse(err));
            } else {
              return resolve(commonFunctions.sendSuccessResponse(res));
            }
          });
        }
      });
    }
  });
}

function changePassword(body) {
  return new Promise(function(resolve, reject) {
    var newBody = _.pick(body, 'agentId', 'currentPassword', 'newPassword');
    if (!newBody.hasOwnProperty('agentId') || !newBody.hasOwnProperty('currentPassword') || !newBody.hasOwnProperty('newPassword') ||
      newBody.agentId === '' || newBody.agentId.trim().length <= 0 || newBody.currentPassword === '' || newBody.currentPassword.trim().length <= 0 || newBody.newPassword === '' || newBody.newPassword.trim().length <= 0) {
      return reject(commonFunctions.sendErrorResponse('Missing parameters'));
    } else {
      User.findOne({
        _id: newBody.agentId
      }, (error, foundUser) => {
        if (error) {
          return reject(commonFunctions.sendErrorResponse(err));
        } else {
          if (foundUser) {
            bcrypt.compare(newBody.currentPassword, foundUser.password, function(err, response) {
              if (err) {
                return reject(commonFunctions.sendErrorResponse('Could not verify password'));
              } else {
                if (response === false) {
                  return reject(commonFunctions.sendErrorResponse('Invalid current password'));
                } else {
                  bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
                    if (err) {
                      return reject(commonFunctions.sendErrorResponse(err));
                    } else {
                      bcrypt.hash(newBody.newPassword, salt, function(err, hash) {
                        if (err) {
                          return reject(commonFunctions.sendErrorResponse(err));
                        } else {
                          var query = {
                            _id: newBody.agentId
                          };
                          User.findOneAndUpdate(query, {
                            $set: {
                              password: hash
                            }
                          }, {
                            new: true
                          }, function(err, user) {
                            if (err) {
                              return reject(commonFunctions.sendErrorResponse(err));
                            } else {
                              var resBody = _.pick(user, 'email', 'firstName', 'lastName', 'role', 'passwordToken');
                              return resolve(commonFunctions.sendSuccessResponse(resBody));
                            }
                          });
                        }
                      });
                    }
                  });
                }
              }
            });
          }
        }
      });
    }
  });
}

function changeAgentPassword(body) {
  return new Promise(function(resolve, reject) {
    var newBody = _.pick(body, 'userId', 'newPassword');
    if (!newBody.hasOwnProperty('userId') || newBody.userId === '' || newBody.userId.trim().length <= 0 ||
      !newBody.hasOwnProperty('userId') || newBody.userId === '' || newBody.userId.trim().length <= 0) {
      return reject(commonFunctions.sendErrorResponse('Missing parameters'));
    } else {
      User.findOne({
        _id: newBody.userId
      }, (error, foundUser) => {
        if (error) {
          return reject(commonFunctions.sendErrorResponse(err));
        } else {
          bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
            if (err) {
              return reject(commonFunctions.sendErrorResponse(err));
            } else {
              bcrypt.hash(newBody.newPassword, salt, function(err, hash) {
                if (err) {
                  return reject(commonFunctions.sendErrorResponse(err));
                } else {
                  var query = {
                    _id: newBody.userId
                  };
                  User.findOneAndUpdate(query, {
                    $set: {
                      password: hash
                    }
                  }, {
                    new: true
                  }, function(err, user) {
                    if (err) {
                      return reject(commonFunctions.sendErrorResponse(err));
                    } else {
                      var resBody = _.pick(user, 'email', 'firstName', 'lastName', 'role', 'passwordToken');
                      return resolve(commonFunctions.sendSuccessResponse(resBody));
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}

function resetPassword(body) {
  return new Promise(function(resolve, reject) {
    var newBody = _.pick(body, 'email');
    if (!newBody.hasOwnProperty('email') || newBody.email === '' || newBody.email.trim().length <= 0) {
      return reject(commonFunctions.sendErrorResponse('Missing parameters'));
    } else {
      User.findOne({
        email: newBody.email
      }, (error, foundUser) => {
        if (error) {
          return reject(commonFunctions.sendErrorResponse(error));
        } else {
          var url = 'http://localhost:3001/resetpassword';
          var token = randtoken(10);
          commonFunctions.createPasswordResetToken(token, foundUser.email).then(function(resetToken) {
            var message = {
              to: foundUser.email,
              from: process.env.SENDGRID_API_EMAIL,
              subject: 'Password Reset',
              text: 'Example text content',
              html: "<p>Dear " + foundUser.firstName + ",</p><p>Please click <a href=" + url + '/' + resetToken + "> here </a> to reset your password. Token is valid for 24 hours only.</p>",
            };
            commonFunctions.sendGridEmail(message).then(function(emailSent) {
              var data = {
                'resetToken': resetToken,
                'email': foundUser.email
              };
              return resolve(commonFunctions.sendSuccessResponse(data));
            }, function(error) {
              return reject(commonFunctions.sendErrorResponse(error));
            }).catch(function(e) {
              return reject(commonFunctions.sendErrorResponse(e));
            });
          }, function(error) {
            return reject(error);
          }).catch(function(e) {
            return reject(e);
          });
        }
      });
    }
  });
}

function resetPasswordChange(body) {
  return new Promise(function(resolve, reject) {
    var newBody = _.pick(body, 'email', 'resetToken', 'newPassword');
    if (!newBody.hasOwnProperty('email') || newBody.email === '' || newBody.email.trim().length <= 0 ||
      !newBody.hasOwnProperty('resetToken') || newBody.resetToken === '' || newBody.resetToken.trim().length <= 0 ||
      !newBody.hasOwnProperty('newPassword') || newBody.newPassword === '' || newBody.newPassword.trim().length <= 0) {
      return reject(commonFunctions.sendErrorResponse('Missing Parameters'));
    } else {
      User.findOne({
        email: newBody.email
      }, (err, user) => {
        if (err) {
          return reject(commonFunctions.sendErrorResponse(err));
        } else {
          if (user.passwordToken !== newBody.resetToken) {
            return reject(commonFunctions.sendErrorResponse('Invalid token'));
          } else {
            bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
              if (err) {
                return reject(commonFunctions.sendErrorResponse(err));
              } else {
                bcrypt.hash(newBody.newPassword, salt, function(err, hash) {
                  if (err) {
                    return reject(commonFunctions.sendErrorResponse(err));
                  } else {
                    var query = {
                      email: newBody.email,
                      passwordToken: newBody.resetToken
                    };
                    User.findOneAndUpdate(query, {
                      $set: {
                        'password': hash,
                        'passwordToken': ''
                      }
                    }, {
                      new: true
                    }, (err, updatedUser) => {
                      if (err) {
                        return reject(commonFunctions.sendErrorResponse(err));
                      } else {
                        var respBody = _.pick(updatedUser, 'email', 'firstName', 'lastName', 'role');
                        return resolve(commonFunctions.sendSuccessResponse('Password successfully update'));
                        // return resolve(commonFunctions.sendSuccessResponse(respBody));
                      }
                    });
                  }
                });
              }
            });
          }
        }
      });
    }
  });
}

function signup(body) {
  return new Promise(function(resolve, reject) {
    var newBody = _.pick(body, 'email', 'firstName', 'lastName', 'role');
    if (Object.keys(newBody).length > 0) {
      if (!newBody.hasOwnProperty('email') || !newBody.hasOwnProperty('firstName') || !newBody.hasOwnProperty('lastName') ||
        !newBody.hasOwnProperty('role') || newBody.email === '' || newBody.firstName === '' || newBody.lastName === '' || newBody.role === '' || newBody.email.trim().length <= 0 ||
        newBody.role.trim().length <= 0 || newBody.firstName.trim().length <= 0 || newBody.lastName.trim().length <= 0
      ) {
        return reject(commonFunctions.sendErrorResponse('Missing Parameters'));
      } else {
        User.findOne({
          email: newBody.email
        }, (err, res) => {
          if (err) {
            return reject(commonFunctions.sendErrorResponse(err));
          } else {
            if (res != null) {
              return reject(commonFunctions.sendErrorResponse('Your email already registered.'));
            } else {
              var randomPassword = randtoken(16);
              bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
                if (err) {
                  return reject(commonFunctions.sendErrorResponse(err));
                } else {
                  bcrypt.hash(randomPassword, salt, function(err, hash) {
                    if (err) {
                      return reject(commonFunctions.sendErrorResponse(err));
                    } else {
                      var addHashPassword = newBody;
                      addHashPassword.password = hash;
                      newBody.verify = '0';
                      const newUser = new User(newBody);
                      newUser.save(function(err, inserted) {
                        if (err) return reject(commonFunctions.sendErrorResponse(err));
                        if (inserted) {
                          var userId;
                          User.find({
                            email: newBody.email
                          }, (err, res) => {
                            if (err) {
                              return reject(commonFunctions.sendErrorResponse(err));
                            } else {
                              userId = res[0]._id;
                              var message = {
                                to: newBody.email,
                                from: process.env.SENDGRID_API_EMAIL,
                                subject: 'Verify Email',
                                text: 'test',
                                html: "<p>Dear " + newBody.firstName + ",</p><p>Please click <a href='http://localhost:4200/#/verify/" + userId + "' target='_blank'> verify email </a> to verify your email.</p>",
                              };
                              commonFunctions.sendGridEmail(message).then(function(emailSent) {
                                return resolve(commonFunctions.sendSuccessResponse(emailSent));
                              }, function(error) {
                                return reject(commonFunctions.sendErrorResponse(error));
                              }).catch(function(e) {
                                return reject(commonFunctions.sendErrorResponse(e));
                              });
                            }
                          }).select('_id');
                          return resolve(commonFunctions.sendSuccessResponse('Please verify your email'));
                        }
                      });
                    }
                  });
                }
              });
            }
          }
        });
      }
    }
  });
}

function verifyUserEmail(body) {
  return new Promise(function(resolve, reject) {
    var newBody = _.pick(body, 'userId');
    if (Object.keys(newBody).length > 0) {
      if (!newBody.hasOwnProperty('userId') || newBody.userId === '' || newBody.userId.trim().length <= 0) {
        return reject(commonFunctions.sendErrorResponse('Missing Parameters'));
      } else {
        var query = {
          '_id': newBody.userId
        };
        User.findOneAndUpdate(query, {
          $set: {
            'verify': '1',
            'createAccount': '0'
          }
        }, (err, updatedUser) => {
          return resolve(commonFunctions.sendSuccessResponse('your email verify'));
        });
      }
    }
  });
}

function docusign(body) {
  return new Promise(function(resolve, reject) {
    var newBody = _.pick(body, 'userId');
    if (Object.keys(newBody).length > 0) {
      if (!newBody.hasOwnProperty('userId') || newBody.userId === '' || newBody.userId.trim().length <= 0) {
        return reject(commonFunctions.sendErrorResponse('Missing Parameters'));
      } else {
        User.findOne({
          '_id': newBody.userId
        }, (err, resp) => {
          if (err) {
            return reject(commonFunctions.sendErrorResponse(err));
          } else {
            Docusign.find({
              _id: '5a2e3fd564a964445fa9234e'
            }, (err, res) => {
              if (err) {
                return reject(commonFunctions.sendErrorResponse(err));
              } else {
                User.find({
                  'role': 'admin'
                }, function(error, response) {
                  if (error) {
                    return reject(commonFunctions.sendErrorResponse(error));
                  } else {
                    commonFunctions.docusignSignup(resp, res[0], response[0]).then(function(docusignRes) {
                      var query = {
                        '_id': newBody.userId
                      };
                      var envelopeId = JSON.parse(docusignRes).envelopeId;
                      var envelopeStatus = JSON.parse(docusignRes).status;
                      User.findOneAndUpdate(query, {
                        $set: {
                          'envelopeId': envelopeId,
                          'envelopeStatus': envelopeStatus
                        }
                      }, (err, updatedUser) => {
                        commonFunctions.docusignGetEnvelope(updatedUser, envelopeId, res[0]).then(function(envelopeRes) {
                          return resolve(commonFunctions.sendSuccessResponse(envelopeRes));
                        }).catch(function(e) {
                          return reject(commonFunctions.sendErrorResponse(e));
                        });
                      });
                    }).catch(function(e) {
                      return reject(commonFunctions.sendErrorResponse(e));
                    });
                  }
                });
              }
            });
          }
        });
      }
    }
  });
}

function docusigndownloadDocument(body) {
  return new Promise(function(resolve, reject) {
    User.find(function(err, res) {
      if (err) {
        return reject(commonFunctions.sendErrorResponse(err));
      } else {
        Docusign.find({
          _id: '5a2e3fd564a964445fa9234e'
        }, (error, response) => {
          if (error) {
            return reject(commonFunctions.sendErrorResponse(error));
          } else {
            User.find({
              'role': 'admin'
            }, function(er, adminRes) {
              if (err) {
                return reject(commonFunctions.sendErrorResponse(err));
              } else {
                for (var i = 0; i < res.length; i++) {
                  if (res[i].createAccount == '0') {
                    if (res[i].envelopeId != undefined) {
                      console.log('_id =', res[i]._id);
                      var user = {
                        'email': res[i].email,
                        'firstName': res[i].firstName,
                        'createAccount': res[i].createAccount
                      };
                      commonFunctions.docusignDownloadSignDocument(user, res[i].envelopeId, response[0]).then(function(docusignRes) {
                        if (docusignRes.user.createAccount == '0') {
                          var filePath = path.join(__dirname, './../public/tmp/' + docusignRes.fileName);
                          console.log('docusignRes', filePath);
                          var adminEmail = res[0].email;
                          // send email with sign document
                          var args = "curl https://api.sendgrid.com/api/mail.send.json \
                                -F to=" + docusignRes.user.email + " -F toname=  -F subject='Account Create' \
                                -F text='testing text body' --form-string html='<p>Dear " + docusignRes.user.firstName + ",</p><p>Your account is created. <a href=" + docusignRes.url.replace(' ', '%20') + ">view aggrement</a></p>' \
                                -F from=" + process.env.SENDGRID_API_EMAIL + " -F api_user=" + process.env.SENDGRID_API_USERNAME + " -F api_key=" + process.env.SENDGRID_API_PASSWORD + " \
                                -F files\[" + filePath + "\]=@" + filePath;

                          exec(args, function(error, stdout, stderr) {
                            if (error !== null) {
                              console.log('exec error: ' + error);
                            }
                            fs.unlink(filePath);
                          });
                          var query = {
                            'email': docusignRes.user.email
                          };
                          User.findOneAndUpdate(query, {
                            $set: {
                              'createAccount': '1',
                              'signDocumentUrl': docusignRes.url
                            }
                          }, (err, updatedUser) => {});
                          return resolve(commonFunctions.sendSuccessResponse('Documents successfully download'));
                        } else {
                          return reject(commonFunctions.sendErrorResponse('No new document'));
                        }
                      }).catch(function(e) {
                        return reject(commonFunctions.sendErrorResponse(e));
                      });
                    }
                  }
                }
              }
            });
          }
        });
      }
    }).select('envelopeId email firstName createAccount');
  });
}
