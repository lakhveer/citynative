require('dotenv').config();
const jwt = require('jsonwebtoken');
const mandrill = require('mandrill-api/mandrill');
const mandrill_client = new mandrill.Mandrill(process.env.MANDRILL_API_KEY);
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);
const User = require('./models/user');
var fs = require('fs');
var randtoken = require('random-token');
var docusign = require('docusign-esign');
var async = require('async');
var request = require('request');

var http = require('http');
var NodeGeocoder = require('node-geocoder');
var options = {
  provider: 'google',
  // Optional depending on the providers
  httpAdapter: 'https', // Default
  apiKey: 'AIzaSyBK0ApCGvSM53_s5Ne1bHAfQGRtxuPSIJU', // for Mapquest, OpenCage, Google Premier
  formatter: null         // 'gpx', 'string', ...
};
var geocoder = NodeGeocoder(options);

var BUCKET_NAME = 'citynative';
var aws = require('aws-sdk');
var configParamsS3 = {accessKeyId:process.env.ACCESS_KEY_ID,secretAccessKey: process.env.SECRET_ACCESS_KEY,region: process.env.REGION};

// docusign integration
var resp = {};
var docusignEnv = 'demo';                    // DocuSign Environment generally demo for testing purposes
var templateId = '0bdd06a5-8bef-4707-8078-5cfff7fc7a14';  // ID of the Template you want to create the Envelope with
var templateRoleName = 'clientSigner';                 // Role Name of the Template
var templateRoleName1 = 'adminSigner';                 // Role Name of the Template
var baseUrl = 'https://' + docusignEnv + '.docusign.net/restapi';
var apiClient = new docusign.ApiClient();

var commonFunctions = {};

commonFunctions.sendErrorResponse = function(body) {
    var message = {};
    message.error = body;
    return JSON.stringify(message);
}

commonFunctions.sendSuccessResponse = function(body) {
    var message = {};
    message.success = body;
    return JSON.stringify(message);
}

commonFunctions.createToken = function(body) {
  return new Promise(function(resolve, reject){
    if( body._id === '' || body.email === '' || body.firstName === '' || body.lastName === '' || body.role === '' || body.email.trim().length <= 0 || body.firstName.trim().length <= 0 || body.lastName.trim().length <= 0 || body.role.trim().length <= 0 ) {
      return reject('Missing parameters');
    } else {
      var payload = { _id: body._id, email: body.email, fName: body.firstName, lName: body.lastName, role: body.role };
      jwt.sign(payload, process.env.TOKEN_SECRET , { algorithm: 'HS256', expiresIn: 12000  }, function(err, token) {
          if(err){
               return reject(err);
          }else{
              return resolve(token);
          }
      });
    }
  });
}

commonFunctions.createPasswordResetToken = function(token, email) {
  return new Promise(function(resolve, reject){
    if( token === '' || token === '' || token.trim().length <= 0 || email === '' || email === '' || email.trim().length <= 0 ) {
      return reject('Missing parameters');
    } else {
      var payload = { token: token, email: email };
      jwt.sign(payload, process.env.TOKEN_SECRET , { algorithm: 'HS256', expiresIn: 86400  }, function(err, jwtToken) {
          if(err){
               return reject(err);
          }else{
            var query = { email: email };
              User.findOneAndUpdate(query, { $set: { passwordToken: jwtToken }}, {new: true}, function(err, updatedToken){
                if(err){
                  return reject(err);
                } else {
                    return resolve(updatedToken.passwordToken);
                }
              });
          }
      });
    }
  });
}

commonFunctions.verifyAdminToken = function(req, res, next) {
  if(!req.body.hasOwnProperty('token') || req.body.token === '' || req.body.token.trim().length <= 0) {
    res.status(400).send(commonFunctions.sendErrorResponse('Missing parameters'));
  } else {
    jwt.verify(req.body.token, process.env.TOKEN_SECRET, function(err, decoded) {
      if(err) {
        res.status(400).send(commonFunctions.sendErrorResponse(err));
      } else {
        if(decoded.role !== 'admin') {
          res.status(400).send(commonFunctions.sendErrorResponse('Invalid role'));
        } else {
          next();
        }
      }
    });
  }
}

commonFunctions.verifyAgentToken = function(req, res, next) {
  if(!req.body.hasOwnProperty('token') || req.body.token === '' || req.body.token.trim().length <= 0) {
    res.status(400).send(commonFunctions.sendErrorResponse('Missing parameters'));
  } else {
    jwt.verify(req.body.token, process.env.TOKEN_SECRET, function(err, decoded) {
      if(err) {
        res.status(400).send(commonFunctions.sendErrorResponse(err));
      } else {
        if(decoded.role !== 'agent') {
          res.status(400).send(commonFunctions.sendErrorResponse('Invalid role'));
        } else {
          next();
        }
      }
    });
  }
}

commonFunctions.verifyUserToken = function(req, res, next) {
  if(!req.body.hasOwnProperty('token') || req.body.token === '' || req.body.token.trim().length <= 0) {
    res.status(400).send(commonFunctions.sendErrorResponse('Missing parameters'));
  } else {
    jwt.verify(req.body.token, process.env.TOKEN_SECRET, function(err, decoded) {
      if(err) {
        res.status(400).send(commonFunctions.sendErrorResponse(err));
      } else {
        if(decoded.role !== 'user') {
          res.status(400).send(commonFunctions.sendErrorResponse('Invalid role'));
        } else {
          next();
        }
      }
    });
  }
}

commonFunctions.sendEmail = function(body) {
  return new Promise(function(resolve, reject){
    var async = false;
    mandrill_client.messages.send({"message": body, "async": async}, function(result) {
      return resolve(result);
    }, function(e) {
      return reject(e);
    });
  });
}

commonFunctions.sendGridEmail = function(body) {
  return new Promise(function(resolve, reject){
    var smail = sgMail.send(body);
    return resolve('email sent');
  });
}

commonFunctions.docusignSignup = function(body, credentials, adminResp) {
  return new Promise(function(resolve, reject){
    var fullName = body.firstName+' ' + body.lastName; // Recipient's Full Name
    var recipientEmail = body.email; // Recipient's Email
    var recipientFirstName = body.firstName;
    var recipientLastName = body.lastName;

    apiClient.setBasePath(baseUrl);
    var creds = JSON.stringify({
      Username: credentials.email,
      Password: credentials.password,
      IntegratorKey: credentials.apiKey
    });
    apiClient.addDefaultHeader('X-DocuSign-Authentication', creds);
    docusign.Configuration.default.setDefaultApiClient(apiClient);
    async.waterfall([
      function login (next) {
        var authApi = new docusign.AuthenticationApi();

        var loginOps = {};
        loginOps.apiPassword = 'true';
        loginOps.includeAccountIdGuid = 'true';
        authApi.login(loginOps, function (err, loginInfo, response) {
          if (err) {
            return next(err);
          }
          if (loginInfo) {
            var loginAccounts = loginInfo.loginAccounts;
            var loginAccount = loginAccounts[0];
            var accountId = loginAccount.accountId;
            var baseUrl = loginAccount.baseUrl;
            var accountDomain = baseUrl.split("/v2");

            apiClient.setBasePath(accountDomain[0]);
            docusign.Configuration.default.setDefaultApiClient(apiClient);
            next(null, loginAccount, credentials);
          }
        });
      },
      function sendTemplate(loginAccount, credentials, next){
        var url  = baseUrl + "/v2/accounts/"+loginAccount.accountId+"/envelopes/";
        var body = JSON.stringify({
        		"emailSubject": "Confirm document sign",
        		"templateId": templateId,
        		"templateRoles": [{
        			"email": recipientEmail,
        			"name": fullName,
        			"roleName": templateRoleName,
              "recipientId":"1",
          		"clientUserId": "1234"
        		},
            {
        			"email": adminResp.email,
        			"name": adminResp.firstName+''+adminResp.lastName,
        			"roleName": templateRoleName1
        		}],
        		"status": "sent"
        	});

          var options = initializeRequest(url, "POST", body, credentials.email, credentials.password, credentials.apiKey);

          request(options, function(err, res, body){
            if(err){
              return reject(err);
            }else{
              return resolve(body);
            }
          });
      }
    ], function end (error) {
      if (error) {
        resp = error;
        return reject(resp);
      }
      process.exit();
    });
  });
  function initializeRequest(url, method, body, email, password, apiKey) {
  	var options = {
  		"method": method,
  		"uri": url,
  		"body": body,
  		"headers": {}
  	};
  	addRequestHeaders(options, email, password, apiKey);
  	return options;
  }
  function addRequestHeaders(options, email, password, apiKey) {
  	dsAuthHeader = JSON.stringify({
  		"Username": email,
  		"Password": password,
  		"IntegratorKey": apiKey	// global
  	});
  	options.headers["X-DocuSign-Authentication"] = dsAuthHeader;
  }
  function parseResponseBody(err, res, body) {
  	if( res.statusCode != 200 && res.statusCode != 201)	{
  		return false;
  	}
  	return true;
  }
}

commonFunctions.docusignGetEnvelope = function(body, envelopeId, credentials) {
  return new Promise(function(resolve, reject){
    apiClient.setBasePath(baseUrl);
    var creds = JSON.stringify({
      Username: credentials.email,
      Password: credentials.password,
      IntegratorKey: credentials.apiKey
    });
    apiClient.addDefaultHeader('X-DocuSign-Authentication', creds);
    docusign.Configuration.default.setDefaultApiClient(apiClient);
    async.waterfall([
      function login (next) {
        var authApi = new docusign.AuthenticationApi();

        var loginOps = {};
        loginOps.apiPassword = 'true';
        loginOps.includeAccountIdGuid = 'true';
        authApi.login(loginOps, function (err, loginInfo, response) {
          if (err) {
            return next(err);
          }
          if (loginInfo) {
            var loginAccounts = loginInfo.loginAccounts;
            var loginAccount = loginAccounts[0];
            var accountId = loginAccount.accountId;
            var baseUrl = loginAccount.baseUrl;
            var accountDomain = baseUrl.split("/v2");

            apiClient.setBasePath(accountDomain[0]);
            docusign.Configuration.default.setDefaultApiClient(apiClient);
            var new_body = body;
            next(null, new_body,envelopeId, loginAccount, credentials);
          }
        });
      },
      function test(new_body,envelopeId, loginAccount, credentials, next){
        var url = baseUrl + "/v2/accounts/"+loginAccount.accountId+"/envelopes/" + envelopeId + "/views/recipient";
        var method = "POST";
        var body = JSON.stringify({
        		"returnUrl": "http://localhost:4200/#/finish",
        		"authenticationMethod": "email",
        		"email": new_body.email,
            "userName": new_body.firstName + ' ' + new_body.lastName,
            "recipientId":"1",
        		"clientUserId": "1234"	// must match clientUserId in step 2!
        	});
        var options = initializeRequest(url, "POST", body, credentials.email, credentials.password, credentials.apiKey);

        request(options, function(err, res, body){
          if(err){
            return reject(err);
          }else{
            return resolve(body);
          }
        });
      }
    ], function end (error) {
      if (error) {
        resp = error;
      }
      process.exit();
    });
  });
function initializeRequest(url, method, body, email, password, apiKey) {
	var options = {
		"method": method,
		"uri": url,
		"body": body,
		"headers": {}
	};
	addRequestHeaders(options, email, password, apiKey);
	return options;
}
function addRequestHeaders(options, email, password, apiKey) {
	dsAuthHeader = JSON.stringify({
		"Username": email,
		"Password": password,
		"IntegratorKey": apiKey	// global
	});
	options.headers["X-DocuSign-Authentication"] = dsAuthHeader;
}
function parseResponseBody(err, res, body) {
	if( res.statusCode != 200 && res.statusCode != 201)	{
		return false;
	}
	return true;
}
}

commonFunctions.docusignDownloadSignDocument = function(user, envelopeId, credentials) {
  return new Promise(function(resolve, reject){
    apiClient.setBasePath(baseUrl);
    var creds = JSON.stringify({
      Username: credentials.email,
      Password: credentials.password,
      IntegratorKey: credentials.apiKey
    });
    apiClient.addDefaultHeader('X-DocuSign-Authentication', creds);
    docusign.Configuration.default.setDefaultApiClient(apiClient);
    async.waterfall([
      function login (next) {
        var authApi = new docusign.AuthenticationApi();

        var loginOps = {};
        loginOps.apiPassword = 'true';
        loginOps.includeAccountIdGuid = 'true';
        authApi.login(loginOps, function (err, loginInfo, response) {
          if (err) {
            return next(err);
          }
          if (loginInfo) {
            var loginAccounts = loginInfo.loginAccounts;
            var loginAccount = loginAccounts[0];
            var accountId = loginAccount.accountId;
            var baseUrl = loginAccount.baseUrl;
            var accountDomain = baseUrl.split("/v2");

            apiClient.setBasePath(accountDomain[0]);
            docusign.Configuration.default.setDefaultApiClient(apiClient);
            next(null, envelopeId, loginAccount, credentials);
          }
        });
      },
      function getEnvlopDocumentList(envelopeId, loginAccount, credentials, next) {
      		var url = baseUrl + "/v2/accounts/"+loginAccount.accountId+"/envelopes/" + envelopeId + "/documents";
      		var body = "";

      		var options = initializeRequest(url, "GET", body, credentials.email, credentials.password, credentials.apiKey);

      		request(options, function(err, res, body) {
      			if(!parseResponseBody(err, res, body)) {
      				return;
      			}
      			next(null, user, body, loginAccount, credentials);
      		});
      },
      function downloadDocument(user, body, loginAccount, credentials, next) {
        var uriList = JSON.parse(body).envelopeDocuments;
        var uris = [], docNames = [], i;
        function downloadEnvelopeDoc(id, body) {
        			uris.push(JSON.parse(body).envelopeDocuments[id].uri);
        			docNames.push(JSON.parse(body).envelopeDocuments[id].name);

        			var url = baseUrl + "/v2/accounts/"+loginAccount.accountId+ uris[id];
        			var body = "";	// no request body for this call

        			var options = initializeRequest(url, "GET", body, credentials.email, credentials.password, credentials.apiKey);

        			options.headers["Accept"] = "application/pdf";
        			options.headers["Content-Transfer-Encoding"] = "base64";
        			request(options, function(err, res, body) {
        				if( res.statusCode != 200 )	// successful GET returns code 200
        				{
        					console.log("\nError downloading document, status is: ", res.statusCode);
        					return;
        				}else{
                  var buffer = new Buffer(body, "base64");
                  var rand = randtoken(8);
                  var saveFile = rand+'_'+docNames[id].replace(' ','_');
                  fs.writeFile('public/tmp/'+saveFile, buffer, function(err) {
                    if( err ) {
                      return;
                    }
                    var returnUrl =  saveDocumentInS3(saveFile);
                    var dat = {"url":returnUrl,"fileName":saveFile,"user":user};
                    return resolve(dat);
                  });
                }
        			});
        }
        for( i = 0; i < 1; i++ )
    		{
    			downloadEnvelopeDoc(i, body);
        }
      }
    ], function end (error) {
      if (error) {
        resp = error;
      }
      process.exit();
    });
  });
  function initializeRequest(url, method, body, email, password, apiKey) {
  	var options = {
  		"method": method,
  		"uri": url,
  		"body": body,
  		"headers": {}
  	};
  	addRequestHeaders(options, email, password, apiKey);
  	return options;
  }
  function addRequestHeaders(options, email, password, apiKey) {
  	dsAuthHeader = JSON.stringify({
  		"Username": email,
  		"Password": password,
  		"IntegratorKey": apiKey	// global
  	});
  	options.headers["X-DocuSign-Authentication"] = dsAuthHeader;
  }
  function parseResponseBody(err, res, body) {
  	if( res.statusCode != 200 && res.statusCode != 201)	{ // success statuses
  		return false;
  	}
  	return true;
  }
  function saveDocumentInS3(fileName){
    var imageNameToSave = fileName;
    var s3 = new aws.S3(configParamsS3);
    var fileName = __dirname + '/public/tmp/' + imageNameToSave;
    var fileBuffer = fs.readFileSync(fileName);
    var metaData = 'application/pdf';
    var saveToDb = 'https://s3-' + process.env.REGION + '.amazonaws.com/' + process.env.BUCKET_NAME + '/' + imageNameToSave;
    s3.putObject({
        ACL: 'public-read',
        Bucket: BUCKET_NAME,
        Key: imageNameToSave,
        Body: fileBuffer,
        ContentType: metaData
    }, function(error, response) {
      if(error){
        return;
      }
    //  fs.unlink(fileName);
    });
    return saveToDb;
  }
}

commonFunctions.docusignCredentials = function(body) {
  return new Promise(function(resolve, reject){
    apiClient.setBasePath(baseUrl);
    var creds = JSON.stringify({
      Username: body.email,
      Password: body.password,
      IntegratorKey: body.apiKey
    });
    apiClient.addDefaultHeader('X-DocuSign-Authentication', creds);
    docusign.Configuration.default.setDefaultApiClient(apiClient);
    async.waterfall([
      function login (next) {
        var authApi = new docusign.AuthenticationApi();
        var loginOps = {};
        loginOps.apiPassword = 'true';
        loginOps.includeAccountIdGuid = 'true';
        authApi.login(loginOps, function (err, loginInfo, response) {
          if (err) {
            return reject(err);
          }
          return resolve(loginInfo.loginAccounts);
        });
      }
    ], function end (error) {
      if (error) {
        resp = error;
      }
      process.exit();
    });
  });
}

commonFunctions.docusignAllAgreements = function(credentials, pageNo) {
  return new Promise(function(resolve, reject){
    apiClient.setBasePath(baseUrl);
    var creds = JSON.stringify({
      Username: credentials.email,
      Password: credentials.password,
      IntegratorKey: credentials.apiKey
    });
    apiClient.addDefaultHeader('X-DocuSign-Authentication', creds);
    docusign.Configuration.default.setDefaultApiClient(apiClient);
    async.waterfall([
      function login (next) {
        var authApi = new docusign.AuthenticationApi();

        var loginOps = {};
        loginOps.apiPassword = 'true';
        loginOps.includeAccountIdGuid = 'true';
        authApi.login(loginOps, function (err, loginInfo, response) {
          if (err) {
            return next(err);
          }
          if (loginInfo) {
            var loginAccounts = loginInfo.loginAccounts;
            var loginAccount = loginAccounts[0];
            var accountId = loginAccount.accountId;
            var baseUrl = loginAccount.baseUrl;
            var accountDomain = baseUrl.split("/v2");

            apiClient.setBasePath(accountDomain[0]);
            docusign.Configuration.default.setDefaultApiClient(apiClient);
            next(null, loginAccount, credentials, pageNo);
          }
        });
      },
      function getEnvlopDocumentList(loginAccount, credentials, pageNo, next) {
      		var url = baseUrl + "/v2/accounts/"+loginAccount.accountId+"/search_folders/completed?start_position="+pageNo+"&include_recipients=true";
      		var body = "";
      		var options = initializeRequest(url, "GET", body, credentials.email, credentials.password, credentials.apiKey);
      		request(options, function(err, res, body) {
      			if(!parseResponseBody(err, res, body)) {
      				return;
      			}
            return resolve(body)
      	//		next(null, body, loginAccount, credentials);
      		});
      }
    ], function end (error) {
      if (error) {
        resp = error;
      }
      process.exit();
    });
  });
  function initializeRequest(url, method, body, email, password, apiKey) {
    var options = {
      "method": method,
      "uri": url,
      "body": body,
      "headers": {}
    };
    addRequestHeaders(options, email, password, apiKey);
    return options;
  }
  function addRequestHeaders(options, email, password, apiKey) {
    dsAuthHeader = JSON.stringify({
      "Username": email,
      "Password": password,
      "IntegratorKey": apiKey	// global
    });
    options.headers["X-DocuSign-Authentication"] = dsAuthHeader;
  }
  function parseResponseBody(err, res, body) {
    if( res.statusCode != 200 && res.statusCode != 201)	{ // success statuses
      return false;
    }
    return true;
  }
}

commonFunctions.googleLatLog = function(zip) {
  return new Promise(function(resolve, reject){
    geocoder.geocode(zip, function ( err, data ) {
      if(err){
        return reject(commonFunctions.sendErrorResponse(err));
      }else{
        return resolve(commonFunctions.sendSuccessResponse(data));
      }
    });
  });
}

module.exports = commonFunctions;
